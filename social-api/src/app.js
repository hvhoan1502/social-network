'use strict';

const express = require("express");
const { json, urlencoded } = require("body-parser");
const cors = require("cors");
const path = require("path");
const http = require("http");

const routes = require("./controllers/index");
// const Translation = require('./helpers/translation');

global.appRoot = path.resolve(__dirname, "../");
global.AppError = require(appRoot + '/lib/error-util').AppError;

const app = express();

app.use(cors());
app.use(json());
app.use(urlencoded({ extended: true }));
app.use(express.static(appRoot + "/public"));

// Load translates
// Translation.init(app);

app.use((req, res, next) => {
  res.onError = (error) =>
    res.status(error.statusCode || 500).send({
      success: false,
      message: error.message,
      code: error.code,
    });
  next();
});

// Api version
app.use('/api', routes);

app.use((req, res, next) => {
  const error = new Error('Endpoint not found');
  res.status(404).send({ success: false, message: error.message });
});

module.exports = { server: http.createServer(app) };
