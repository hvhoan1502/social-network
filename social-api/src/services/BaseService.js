'use strict';

const Constants = require('../common/Constants');
const Validator = require('../helpers/validators');
const Promise = require('../../lib/promise');
const _ = require('lodash');
class BaseService {
	constructor(options = {}) {
		this.model = options.model;
		this.deletedMode = options.deletedMode || Constants.DELETE_MODE.Soft;
		this.validation = Validator;
	}

	/**
	 *
	 * @param {*} id
	 * @param {*} options
	 * @returns
	 */
	async findById(id, options = {}) {
		this.validation.validateObjectIds(id);
		let searchQuery = this.getAssociationFields(this.model.findById(id, options));
		const record = await searchQuery;
		this.validation.validateRecordExist(record);

		return record;
	}

	/**
	 *
	 * @param {*} queryObject
	 * @param {*} options
	 * @returns
	 */
	async find(queryObject, options = {}) {
		let searchQuery = this.getAssociationFields(this.model.find(queryObject, options, { lean: true }));
		const records = await searchQuery;
		this.validation.validateRecordExist(records);
		return records;
	}

	/**
	 *
	 * @param {*} queryObject
	 * @returns
	 */
	async findOne(queryObject) {
		let searchQuery = this.getAssociationFields(this.model.findOne(queryObject));
		const record = await searchQuery;
		this.validation.validateRecordExist(record);

		return record;
	}

	/**
	 *
	 * @param {*} data
	 * @param {*} action
	 */
	save(data, action = Constants.ACTION.INSERT) {
		// TO DO
	}

	/**
	 *
	 * @param {*} queryParams
	 * @returns
	 */
	async findOneAndRemove(queryParams = {}) {
		this.validation.validateParamsExist(queryParams);
		const deletedRecord = await this.model.findOneAndRemove(queryParams);
		this.validation.validateRecordExist(deletedRecord);

		return deletedRecord;
	}

	/**
	 *
	 * @param {*} id
	 * @param {*} updateObject
	 * @returns
	 */
	async findByIdAndUpdate(id, updateObject = {}, options = {}) {
		this.validation.validateObjectIds(id);
		const updatedRecord = await this.model.findByIdAndUpdate(id, updateObject, options);
		this.validation.validateRecordExist(updatedRecord);

		return updatedRecord;
	}

	/**
	 *
	 * @param {*} id
	 * @param {*} updateObject
	 * @param {*} options
	 */
	async findOneAndUpdate(queryObject, updateObject, options = {}) {
		_.merge(options, { useFindAndModify: false });
		const updatedRecord = await this.model.findOneAndUpdate(queryObject, updateObject, options);
		this.validation.validateRecordExist(updatedRecord);

		return updatedRecord;
	}

	/**
	 *
	 * @param {*} queryObject
	 * @returns
	 */
	remove(queryObject) {
		return this.model.remove(queryObject);
	}

	findByIdAndRemove(searchParams) {
		this.model.findOneAndRemove(searchParams);
	}
	/**
	 * This func allow override from child class
	 * @param {*} searchPromise
	 */
	getAssociationFields(searchPromise) {
		return Promise.resolve(searchPromise);
	}
}

module.exports = { BaseService };
