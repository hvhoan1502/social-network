'use strict';

const mongoose = require('mongoose');

const { BaseService } = require('./BaseService');
const { UserService } = require('./UserService');
const { StoryModel } = require('../models/StoryModel');
const { CommentModel } = require('../models/CommentModel');
const { Resize } = require('../helpers/resize');

class StoryService extends BaseService {
	constructor() {
		super({
			model: StoryModel
		});
		this.userService = new UserService();
	}

	async getAllStory() {
		let stories = await this.find({});
		// console.log(stories)
		for (let storyIndex = 0; storyIndex < stories.length; storyIndex++) {
			const story = stories[storyIndex];
			const commentCount = story.comments.length
			stories[storyIndex].commentNumber = commentCount;
			stories[storyIndex].comments = commentCount ? [story.comments[0]] : [];
		}

		return stories;
	}

	async getStoryByAuthorId(authorId) {
		return await this.find({ author: { $ne: authorId } });
	}

	async createStory(content, userId, image) {
		this.validation.validateObjectIds(userId);
		try {
			let storyContent = { content, author: userId, image };
			if (image) {
				const storyPath = path.join(appRoot, '/public/images/story');
				const fileUpload = new Resize(storyPath);

				const fileName = await fileUpload.save(image.buffer, image.originalname);

				storyContent.image = fileName;
			}
			var story = new this.model(storyContent);
			await story.save();
			const user = await this.userService.findByIdAndUpdate(userId, { $push: { stories: story._id } });
			this.validation.validateUserExist(user);
			return this.model.populate(story, { path: 'author', select: 'name' });
		} catch (error) {
			await this.findByIdAndRemove(story);
			if (error instanceof AppError) throw error;
			const InvalidStoryInfo = Constants.VALIDATION_INFO.InvalidStoryInfo;
			throw new AppError(InvalidStoryInfo.Message, InvalidStoryInfo.StatusCode, InvalidStoryInfo.Code);
		}
	}

	async updateStory(idStory, idUser, content) {
		this.validation.validateObjectIds(idStory, idUser);
		this.validation.validateParamsExist(content, InvalidStoryInfo.Message, InvalidStoryInfo.Code);

		return await this.findOneAndUpdate({ _id: idStory, author: idUser }, { content }, { new: true });
	}

	async removeStory(idStory, idUser) {
		this.validation.validateObjectIds(idStory, idUser);
		const story = await this.findOneAndRemove({ _id: idStory, author: idUser });

		await mongoose.model('Comment').remove({ story: idStory });
		await this.userService.findByIdAndUpdate(idUser, { $pull: { stories: idStory } });

		return story;
	}

	async likeStory(idUser, idStory) {
		this.validation.validateObjectIds(idStory, idUser);
		const updateObj = { $addToSet: { fans: idUser } };
		const queryObj = { _id: idStory, fans: { $ne: idUser } };
		const story = await this.findOneAndUpdate(queryObj, updateObj, { new: true });

		return story;
	}

	async dislikeStory(idUser, idStory) {
		this.validation.validateObjectIds(idStory, idUser);
		const updateObj = { $pull: { fans: idUser } };
		const queryObj = { _id: idStory, fans: { $all: [idUser] } };
		const story = await this.findOneAndUpdate(queryObj, updateObj, { new: true });

		return story;
	}

	/**
	 *
	 * @param {*} searchQuery
	 */
	getAssociationFields(searchQuery) {
		searchQuery
			.populate({ path: 'author', select: ['name', 'avatar'] })
			.populate({ path: 'fans', select: 'name' })
			.populate({
				path: 'comments',
				populate: { path: 'author', select: ['name', 'avatar']},
				sort: { 'createdAt': 'DESC' }
			});

		return Promise.resolve(searchQuery);
	}
}

module.exports = { StoryService };
