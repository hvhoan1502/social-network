'use strict';

const { BaseService } = require('./BaseService');
const { RoomModel } = require('../models/RoomModel');
const { StoryService } = require('../services/StoryService');

class RoomService extends BaseService {
	constructor() {
		super({
			model: RoomModel
		});
		this.storyService = new StoryService();
	}

	async createChatRoom(authorId, friendId) {
		this.validation.validateObjectIds(authorId, friendId);
		try {
			var chatRoom = new this.model({ author: authorId, users: [authorId, friendId] });
			return await chatRoom.save();
		} catch (error) {
			await this.storyService.findByIdAndRemove(chatRoom);
			if (error instanceof AppError) throw error;
			throw new AppError('Invalid chat room info.', 400, 'INVALID_CHAT_ROOM_INFO');
		}
	}

	async getChatRoomsByUserId(userId) {
		this.validation.validateObjectIds(userId);
		return await this.find({ users: { $in: [userId] } });
	}

	async getChatRoomById(roomId) {
		this.validation.validateObjectIds(roomId);
		return await this.findOne({ _id: roomId });
	}

	async getChatRoomByAuthorId(authorId) {
		this.validation.validateObjectIds(authorId);
		return await this.findOne({ author: { $ne: authorId } });
	}

	async getAvailableRoom(users) {
		this.validation.validateObjectIds(users);
		return await this.find({
			users: {
				$size: users.length,
				$all: [...users]
			}
		});
	}

	async getAssociationFields(searchQuery) {
		searchQuery.populate({ path: 'users', select: ['name', 'avatar'] }).populate({ path: 'messages', select: ['message', 'author'] });

		return searchQuery;
	}
}

module.exports = { RoomService };
