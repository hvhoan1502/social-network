'use strict';

const { BaseService } = require('./BaseService');
const { UserModel } = require('../models/UserModel');
const Promise = require('../../lib/promise');
const { hash, compare } = require('bcrypt');
const { sign, verify } = require('../helpers/jwt');
const path = require('path');

class UserService extends BaseService {
	constructor() {
		super({
			model: UserModel
		});
	}

	async signUp(name, email, plainPassword, birthday) {
		this.validation.validateParamsExist(plainPassword, 'Invalid user info.', 'INVALID_USER_INFO');
		const password = await hash(plainPassword, 8);
		try {
			const user = new this.model({ name, email, password, birthday });
			await user.save();

			return user;
		} catch (error) {
			if (error.code) throw new AppError('Email existed.', 400, 'EMAIL_EXISTED');
			throw new AppError('Invalid user info.', 400, 'INVALID_USER_INFO');
		}
	}

	async signIn(email, password) {
		const user = await this.findOne({ email });
		const isSame = await compare(password, user.password);
		this.validation.validateParamsExist(isSame, 'Invalid user info', 'INVALID_USER_INFO');
		const userInfo = user.toObject();
		const token = await sign({ _id: userInfo._id });
		userInfo.token = token;
		delete userInfo.password;
		return userInfo;
	}

	async checkSignInStatus(token) {
		const { _id } = await verify(token);
		this.validation.validateObjectIds(_id);
		const user = await this.findById(_id);
		const userInfo = user.toObject();
		const newToken = await sign({ _id: userInfo._id });
		userInfo.token = newToken;
		delete userInfo.password;
		return userInfo;
	}

	async getUserById(id) {
		this.validation.validateObjectIds(id);
		const user = await this.findById(id);

		delete user.password;
		return user;
	}

	async updateProfile(userId, body) {
		this.validation.validateObjectIds(userId);
		if (!body) {
			throw new AppError('Body should not be empty.', 400, 'CONTENT_NOT_EMPTY');
		}

		body.newPassword ? (body.password = await hash(body.newPassword, 8)) : true;

		if (body.avatarFile) {
			const avatarPath = path.join(appRoot, '/public/images/avatars');
			const fileUpload = new Resize(avatarPath);

			const fileName = await fileUpload.save(body.avatarFile.buffer, body.avatarFile.originalname);
			body.avatar = fileName;
		}

		delete body.newPassword;
		delete body.avatarFile;
		delete body.image;

		const existingUser = await this.findOneAndUpdate({ _id: userId }, body, { new: true });

		const userInfo = existingUser.toObject();
		const token = await sign({ _id: userInfo._id });
		userInfo.token = token;
		delete userInfo.password;
		return userInfo;
	}

	getAssociationFields(searchQuery) {
		searchQuery.populate({ path: 'stories', populate: { path: 'comments' } });

		return Promise.resolve(searchQuery);
	}
}

module.exports = { UserService };
