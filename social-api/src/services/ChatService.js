'use strict';

const { BaseService } = require('./BaseService');
const { ChatModel } = require('../models/ChatModel');

class ChatService extends BaseService {
	constructor() {
		super({
			model: ChatModel
		});
	}

	async createChatMessage(roomId, message, authorId) {
		this.validation.validateObjectIds([roomId, authorId]);
		try {
			const chatMessage = new this.model({ room: roomId, message, author: authorId });
			return await chatMessage.save();
		} catch (err) {
			if (error instanceof Error) throw error;
			throw new AppError('Invalid chat message info.', 400, 'INVALID_CHAT_MESSAGE_INFO');
		}
	}
}

module.exports = { ChatService };
