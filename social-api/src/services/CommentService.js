'use strict';
const { BaseService } = require('./BaseService');
const { StoryService } = require('../services/StoryService');
const { CommentModel } = require('../models/CommentModel');
class CommentService extends BaseService {
	constructor() {
		super({
			model: CommentModel
		});

		this.storyService = new StoryService();
	}

	async createComment(idUser, idStory, content) {
		this.validation.validateObjectIds(idUser, idStory);
		this.validation.validateParamsExist(content, 'Content should not be empty.', 'CONTENT_NOT_EMPTY');
		const comment = new this.model({ content, author: idUser, story: idStory });
		const updateObject = { $push: { comments: comment._id } };
		const story = await this.storyService.findByIdAndUpdate(idStory, updateObject);
		this.validation.validateStoryExist(story);
		await comment.save();
		return this.model.populate(comment, { path: 'author', select: ['name', 'avatar'] });
	}

	async removeComment(idUser, idComment) {
		this.validation.validateObjectIds(idUser, idComment);
		const comment = await this.findOneAndRemove({ _id: idComment, author: idUser });
		await this.storyService.findByIdAndUpdate(comment.story._id, { $pull: { comments: idComment } });
		return comment;
	}

	async likeComment(idUser, idComment) {
		this.validation.validateObjectIds(idComment, idUser);
		const updateObj = { $addToSet: { fans: idUser } };
		const queryObj = { _id: idComment, fans: { $ne: idUser } };
		const comment = await this.findOneAndUpdate(queryObj, updateObj, { new: true });

		return comment;
	}

	async dislikeComment(idUser, idComment) {
		this.validation.validateObjectIds(idComment, idUser);
		const updateObj = { $pull: { fans: idUser } };
		const queryObj = { _id: idComment, fans: { $all: [idUser] } };
		const comment = await this.findOneAndUpdate(queryObj, updateObj, { new: true });

		return comment;
	}

	async loadComment(body) {
		const { pageIndex = 1, pageNumber = 5, storyId } = body;
		this.validation.validateObjectIds(storyId);
		const commentLoaded = (pageIndex - 1) * pageNumber + 1;
		const comments = await this.model.find({ story: storyId })
										.sort({ 'createdAt': 'DESC' })
										.skip(commentLoaded)
										.limit(pageNumber);
		this.validation.validateRecordExist(comments);

		console.log(pageIndex, pageNumber, commentLoaded);
		console.log('Data: ')
		console.log(comments)

		return comments;
	}
}

module.exports = { CommentService };
