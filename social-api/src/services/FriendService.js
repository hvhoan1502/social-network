'use strict';

const { UserService } = require('../services/UserService');
const Validator = require('../helpers/validators');

class FriendService {
	constructor() {
		this.userService = new UserService();
		this.validation = Validator;
	}
	async sendFriendRequest(idUser, idReceiver) {
		this.validation.validateObjectIds(idUser, idReceiver);
		const queryObj = {
			_id: idUser,
			friends: { $ne: idReceiver },
			incomingRequests: { $ne: idReceiver },
			sentRequests: { $ne: idReceiver }
		};
		const updateObj = { $push: { sentRequests: idReceiver } };
		await this.userService.findOneAndUpdate(queryObj, updateObj, { new: true });

		const receiver = await this.userService.findByIdAndUpdate(idReceiver, { $push: { incomingRequests: idUser } }, { fields: { name: 1, avatar: 1 } });

		return receiver;
	}

	async removeFriendRequest(idUser, idReceiver) {
		this.validation.validateObjectIds(idUser, idReceiver);
		const user = await this.userService.findByIdAndUpdate(idUser, { $pull: { sentRequests: idReceiver } });

		const queryObj = {
			_id: idReceiver,
			incomingRequests: { $eq: idUser }
		};
		const updateObj = { $pull: { incomingRequests: idUser } };
		const receiver = await this.userService.findOneAndUpdate(queryObj, updateObj, { fields: { name: 1, avatar: 1 } });

		return receiver;
	}

	async acceptRequest(idUser, idRequestUser) {
		this.validation.validateObjectIds(idUser, idRequestUser);
		const queryUser = {
			_id: idUser,
			incomingRequests: { $eq: idRequestUser }
		};
		const updateUser = {
			$pull: { incomingRequests: idRequestUser },
			$push: { friends: idRequestUser }
		};
		await this.userService.findOneAndUpdate(queryUser, updateUser);

		const queryFriend = {
			_id: idRequestUser,
			sentRequests: { $eq: idUser }
		};
		const updateFriend = {
			$pull: { sentRequests: idUser },
			$push: { friends: idUser }
		};
		const friend = await this.userService.findOneAndUpdate(queryFriend, updateFriend, { fields: { name: 1, avatar: 1, email: 1 } });

		return friend;
	}

	async declineRequest(idUser, idRequestUser) {
		this.validation.validateObjectIds(idUser, idRequestUser);
		const queryUser = {
			_id: idUser,
			incomingRequests: { $eq: idRequestUser }
		};
		const updateUser = {
			$pull: { incomingRequests: idRequestUser }
		};
		await this.userService.findOneAndUpdate(queryUser, updateUser);
		const queryRequestor = {
			_id: idRequestUser,
			sentRequests: { $eq: idUser }
		};
		const updateRequestor = {
			$pull: { sentRequests: idUser }
		};
		const requestor = await this.userService.findOneAndUpdate(queryRequestor, updateRequestor, { fields: { name: 1, avatar: 1 } });

		return requestor;
	}

	async removeFriend(idUser, idFriend) {
		this.validation.validateObjectIds(idUser, idFriend);
		const queryUser = {
			_id: idUser,
			friends: { $eq: idFriend }
		};
		const updateUser = {
			$pull: { friends: idFriend }
		};
		await this.userService.findOneAndUpdate(queryUser, updateUser);
		const queryFriend = {
			_id: idFriend,
			friends: { $eq: idUser }
		};
		const updateFriend = {
			$pull: { friends: idUser }
		};
		const friend = await this.userService.findOneAndUpdate(queryFriend, updateFriend);

		return friend;
	}

	async getUsers(idUser) {
		this.validation.validateObjectIds(idUser);
		const { friends } = await this.userService.model.findById(idUser, { friends: 1 }).populate('friends', ['name', 'avatar', 'email']);
		const { sentRequests } = await this.userService.model.findById(idUser, { sentRequests: 1 }).populate('sentRequests', ['name', 'avatar', 'email']);
		const { incomingRequests } = await this.userService.model.findById(idUser, { incomingRequests: 1 }).populate('incomingRequests', ['name', 'avatar', 'email']);
		const knownUsers = friends.concat(sentRequests).concat(incomingRequests);
		const _idKnownUsers = knownUsers.map((u) => u._id).concat(idUser);
		const otherUsers = await this.userService.find({ _id: { $nin: _idKnownUsers } }, { name: 1 });
		return { friends, sentRequests, incomingRequests, otherUsers };
	}
}

module.exports = { FriendService };
