'use strict';

const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
	email: { type: String, trim: true, required: true, unique: true },
	password: { type: String, trim: true, required: true },
	name: { type: String, trim: true, required: true },
	birthday: { type: Date, required: true },
	description: { type: String, trim: true },
	avatar: { type: String, trim: true },
	stories: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Story' }],
	friends: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
	incomingRequests: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
	sentRequests: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }]
}, { timestamps: true });

module.exports = { UserModel: mongoose.model('User', userSchema) };
