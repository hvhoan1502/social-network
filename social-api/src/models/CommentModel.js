'use strict';

const mongoose = require('mongoose');

const commentSchema = new mongoose.Schema({
	author: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
	content: { type: String, trim: true, required: true },
	created_date: { type: Date, required: true, default: new Date() },
	story: { type: String, trim: true, required: true },
	fans: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }]
}, { timestamps: true });

module.exports = { CommentModel: mongoose.model('Comment', commentSchema) };
