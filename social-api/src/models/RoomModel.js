'use strict';

const mongoose = require('mongoose');

const roomSchema = mongoose.Schema(
	{
		author: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
		users: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
		messages: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Chat' }]
	},
	{ timestamps: true }
);

module.exports = { RoomModel: mongoose.model('Room', roomSchema) };
