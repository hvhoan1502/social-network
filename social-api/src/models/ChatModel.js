'use strict';

const mongoose = require('mongoose');
const Constants = require('../common/Constants');

const messageSchema = mongoose.Schema(
	{
		room: { type: String, required: true, trim: true },
		message: { type: String, required: true, unique: true },
		type: { type: String, required: true, unique: true, default: Constants.MESSAGE_TYPES.TYPE_TEXT },
		author: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
		readByRecipients: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }]
	},
	{ timestamps: true }
);

module.exports = { ChatModel: mongoose.model('Chat', messageSchema) };
