'use strict';

const mongoose = require('mongoose');

const storySchema = new mongoose.Schema({
	author: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
	content: { type: String, trim: true, required: true },
	image: { type: String, trim: true },
	created_date: { type: Date, required: true, default: new Date() },
	fans: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
	comments: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Comment' }]
}, { timestamps: true });

module.exports = { StoryModel: mongoose.model('Story', storySchema) };
