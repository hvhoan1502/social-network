ERROR_MESSAGE = {
    InvalidId: 'Invalid Object Id.',
    CannotFindStory: 'Cannot Find Story.',
    CannotFindUser: 'Cannot Find User.',
    InvalidStoryInfo: 'Invalid Story Information',
    ContentNotEmpty: 'Content Should Not Be Empty',
    IdNotEmpty: 'Id Not Empty',
    ParamsNotEmpty: 'Params Not Empty'
}

ERROR_CODE = {
    InvalidId: 'INVALID_ID',
    CannotFindStory: 'CANNOT_FIND_STORY',
    CannotFindUser: 'CANNOT_FIND_USER',
    InvalidStoryInfo: 'INVALID_STORY_INFO',
    ContentNotEmpty: 'CONTENT_NOT_EMPTY',
    IdNotEmpty: 'ID_NOT_EMPTY',
    ParamsNotEmpty: 'PARAMS_NOT_EMPTY'
}

const STATUS_CODE = {
    BadRequest: 400,
    Unauthorized: 401,
    Forbidden: 403,
    NotFound: 404,
    InternalServerError: 500,
    NotImplemented: 501,
    ServiceUnavailable: 502
}

const VALIDATION_INFO = {
    InvalidObjectId: {
        Message: ERROR_MESSAGE.InvalidId,
        StatusCode: STATUS_CODE.BadRequest,
        Code: ERROR_CODE.InvalidId
    },
    CannotFindStory: {
        Message: ERROR_MESSAGE.CannotFindStory,
        StatusCode: STATUS_CODE.BadRequest,
        Code: ERROR_CODE.CannotFindStory
    },
    CannotFindUser: {
        Message: ERROR_MESSAGE.CannotFindUser,
        StatusCode: STATUS_CODE.BadRequest,
        Code: ERROR_CODE.CannotFindUser
    },
    InvalidStoryInfo: {
        Message: ERROR_MESSAGE.InvalidStoryInfo,
        StatusCode: STATUS_CODE.BadRequest,
        Code: ERROR_CODE.InvalidStoryInfo
    },
    ContentNotEmpty: {
        Message: ERROR_MESSAGE.ContentNotEmpty,
        StatusCode: STATUS_CODE.Forbidden,
        Code: ERROR_CODE.ContentNotEmpty
    },
    ParamsNotEmpty: {
        Message: ERROR_MESSAGE.ParamsNotEmpty,
        StatusCode: STATUS_CODE.Forbidden,
        Code: ERROR_CODE.ParamsNotEmpty
    }
}

const MESSAGE_TYPES = {
    TYPE_TEXT: "text",
};

const SOCKET_ON_TYPES = {
    Typing: 'typing',
    StopTyping: 'stopTyping',
    ChatMessage: 'chatMessage'
}

const SOCKET_EMIT_TYPES = {
    ReceivedMessage: 'receivedMessage',
    NotifyTyping: 'notifyTyping',
    NotifyStopTyping: 'notifyStopTyping'
}

const DELETE_MODE = {
    Soft: 'soft',
    Hard: 'hard',
}

const ACTION = {
    Create: 'create',
    Update: 'update',
    Delete: 'delete',
    Search: 'search'
}

module.exports = {
    MESSAGE_TYPES,
    SOCKET_ON_TYPES,
    SOCKET_EMIT_TYPES,
    STATUS_CODE,
    ERROR_CODE,
    ERROR_MESSAGE,
    VALIDATION_INFO,
    DELETE_MODE
}