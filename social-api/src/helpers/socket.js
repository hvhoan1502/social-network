'use strict';

const Constants = require('../common/Constants');
const messages = [];

const socket = (server) => {
  global.io = require("socket.io")(server, {
    cors: {
      origin: "http://localhost:4200",
      methods: ["GET", "POST"],
    },
  });

  global.io.on("connection", (socket) => {
    let users = [];
    console.log("User is connected");
    socket.on("disconnect", () => {
      console.log("Disconnected");
    });

    socket.on("identity", (userId) => {
      console.log('Identity...');
      users.push({
        socketId: socket.id,
        userId: userId,
      });
    });
    console.log(socket.id);

    //Someone is typing
    socket.on(Constants.SOCKET_ON_TYPES.Typing, (data) => {
      console.log("Data:  ");
      console.log(data);
      socket.broadcast.emit(Constants.SOCKET_EMIT_TYPES.NotifyTyping, {
        user: data.user || "default",
        message: data.message || "is typing",
      });
    });

    //when someone stops typing
    socket.on(Constants.SOCKET_ON_TYPES.StopTyping, () => {
      console.log('Stop typing.')
      socket.broadcast.emit(Constants.SOCKET_EMIT_TYPES.NotifyStopTyping, true);
    });

    socket.on(Constants.SOCKET_ON_TYPES.ChatMessage, function (msg) {
      messages.push(msg);
      console.log("messages: ", messages);
      //broadcast message to everyone in port:5000 except yourself.
      io.emit(Constants.SOCKET_EMIT_TYPES.ReceivedMessage, messages);
      socket.emit(Constants.SOCKET_EMIT_TYPES.ReceivedMessage, messages);
    });
  });
};

module.exports = { socket };
