'use strict';

const DateUtils = require('../../lib/date-util');

const info = (namespace, message, object = null) => {
	if (object) {
		console.info(`[${DateUtils.getTimestamp()}] [INFO] [${namespace}] ${message}`, object);
	} else {
		console.info(`[${DateUtils.getTimestamp()}] [INFO] [${namespace}] ${message}`);
	}
};

const warn = (namespace, message, object = null) => {
	if (object) {
		console.warn(`[${DateUtils.getTimestamp()}] [WARN] [${namespace}] ${message}`, object);
	} else {
		console.warn(`[${DateUtils.getTimestamp()}] [WARN] [${namespace}] ${message}`);
	}
};

const error = (namespace, message, object = null) => {
	if (object) {
		console.error(`[${DateUtils.getTimestamp()}] [ERROR] [${namespace}] ${message}`, object);
	} else {
		console.error(`[${DateUtils.getTimestamp()}] [ERROR] [${namespace}] ${message}`);
	}
};

const debug = (namespace, message, object = null) => {
	if (object) {
		console.debug(`[${DateUtils.getTimestamp()}] [DEBUG] [${namespace}] ${message}`, object);
	} else {
		console.debug(`[${DateUtils.getTimestamp()}] [DEBUG] [${namespace}] ${message}`);
	}
};


module.exports = {
	info,
	warn,
	error,
	debug
};