const i18next = require('i18next');
const Backend = require('i18next-node-fs-backend');
const i18nextMiddleware = require('i18next-express-middleware');


const init = (app) => {
    i18next.use(Backend)
        . use(i18nextMiddleware.LanguageDetector)
        .init({
            backend: {
                loadPath: appRoot + '/locales/{{lng}}/{{ns}}.json'
            },
            fallbackLng: 'en',
            preload: ['en']
        });
    app.use(i18nextMiddleware.handler(i18next));
}

module.exports = { init };

