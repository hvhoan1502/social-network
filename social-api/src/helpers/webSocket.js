'use strict';
class WebSockets {
  // users = [];
  connection(client) {
    // event fired when the chat room is disconnected
    console.log('User is connected');
    client.on("disconnect", () => {
      // this.users = this.users.filter((user) => user.socketId !== client.id);
      console.log('Disconnected');
    });
    // // add identity of user mapped to the socket id
    client.on("identity", (userId) => {
      this.users.push({
        socketId: client.id,
        userId: userId,
      });
    });
    // // subscribe person to chat & other user as well
    // client.on("subscribe", (room, otherUserId = "") => {
    //   this.subscribeOtherUser(room, otherUserId);
    //   client.join(room);
    // });
    // // mute a chat room
    // client.on("unsubscribe", (room) => {
    //   client.leave(room);
    // });
    //Someone is typing
    client.on("typing", (data) => {
      client.broadcast.emit("notifyTyping", {
        user: data.user,
        message: data.message,
      });
    });

    //when soemone stops typing
    client.on("stopTyping", () => {
      client.broadcast.emit("notifyStopTyping");
    });

    client.on("chat message", function (msg) {
      console.log("message: " + msg);

      //broadcast message to everyone in port:5000 except yourself.
      client.broadcast.emit("received", { message: msg });

      //save chat to the database
      // connect.then((db) => {
      //   console.log("connected correctly to the server");
      //   let chatMessage = new Chat({ message: msg, sender: "Anonymous" });

      //   chatMessage.save();
      // });
    });
  }

  subscribeOtherUser(room, otherUserId) {
    const userSockets = this.users.filter(
      (user) => user.userId === otherUserId
    );
    userSockets.map((userInfo) => {
      const socketConn = global.io.sockets.connected(userInfo.socketId);
      if (socketConn) {
        socketConn.join(room);
      }
    });
  }
}

module.exports = WebSockets;
