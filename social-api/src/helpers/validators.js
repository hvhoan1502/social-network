'use strict';

const mongoose = require('mongoose');
const { VALIDATION_INFO, c } = require('../common/Constants');
class Validator {
    static validateObjectIds(...ids) {
        ids.forEach(id => {
            try {
                new mongoose.Types.ObjectId(id);
            } catch (error) {
                throw new AppError(VALIDATION_INFO.InvalidObjectId.Message, 
                            VALIDATION_INFO.InvalidObjectId.StatusCode, VALIDATION_INFO.InvalidObjectId.Code);
            }
        });
    }

    static validateParamsExist(param, message = VALIDATION_INFO.ParamsNotEmpty.Message, code = VALIDATION_INFO.ParamsNotEmpty.Code) {
        if (!param) {
            throw new AppError(message, VALIDATION_INFO.ParamsNotEmpty.StatusCode, code);
        }
    }
    
    static validateObjectArrIds(ids) {
        ids.forEach(id => {
            try {
                new mongoose.Types.ObjectId(id);
            } catch (error) {
                throw new AppError(VALIDATION_INFO.InvalidObjectId.Message, 
                    VALIDATION_INFO.InvalidObjectId.StatusCode, VALIDATION_INFO.InvalidObjectId.Code);
            }
        });
    }
    
    static validateStoryExist(story) {
        if (!story) {
            throw new AppError(VALIDATION_INFO.CannotFindStory.Message, 
                VALIDATION_INFO.CannotFindStory.StatusCode, VALIDATION_INFO.CannotFindStory.Code);
        }
    }
    
    static validateUserExist(user) {
        if (!user) {
            throw new AppError(VALIDATION_INFO.CannotFindUser.Message, 
                VALIDATION_INFO.CannotFindUser.StatusCode, VALIDATION_INFO.CannotFindUser.Code);
        }
    }

    /**
     * 
     * @param {*} record 
     * @param {*} recordName 
     */
    static validateRecordExist(record, recordName=null) {
        if (!record) {
            const validationDetail = Constants.VALIDATION_INFO.ParamsNotEmpty;
            // Add logger error
            throw new AppError(validationDetail.Message, Constants.STATUS_CODE.Forbidden, validationDetail.Code);
        }
    }
}

module.exports = Validator;
