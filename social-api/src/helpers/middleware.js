'use strict';

const { verify } = require('./jwt');

class Middleware {
    static async mustBeUser(req, res, next) {
        try {
            const { token } = req.headers;
            const { _id } = await verify(token);
            req.idUser = _id;
            next();
        } catch (error) {
            res.onError(error);
        }
    }
    
    static uploadMiddleWare() {
        const multer = require('multer');
    
        return multer({
            limits: {
                fileSize: 4 * 1024 * 1024
            }
        });
    }
}

module.exports = Middleware;
