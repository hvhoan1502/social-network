'use strict';

const jwt = require('jsonwebtoken');
const Config = require('../../config/index');

const SECRET_KET = Config.server.secretKey; 
class Jwt {
    static sign(obj) {
        return new Promise((resolve, reject) => {
            jwt.sign(obj, SECRET_KET, { expiresIn: 6000 }, (error, token) => {
                if (error) return reject(error);
                resolve(token);
            });
        });
    }
    
    static verify(token) {
        return new Promise((resolve, reject) => {
            jwt.verify(token, SECRET_KET, (error, obj) => {
                if (error) return reject(new AppError(error.message, 400, 'INVALID_TOKEN'));
                delete obj.exp;
                delete obj.iat;
                resolve(obj);
            });
        });
    }
}

module.exports = Jwt;
