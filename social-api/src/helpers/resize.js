'use strict';

const sharp = require('sharp');
const path = require('path');

class Resize {
    constructor(folder) {
        this.folder = folder;
    }

    async save(buffer, fileName) {
        const filePath = this.filePath(fileName);

        await sharp(buffer)
                .resize(1080, 1080, {
                    fit: sharp.fit.inside,
                    withoutEnlargement: true
                }).toFile(filePath);
        
        return fileName;
    }

    filePath(name) {
        return path.resolve(`${this.folder}/${name}`);
    }
}

module.exports = { Resize };