'use strict';

const Config = require('../../config/index');

const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
mongoose.set('useFindAndModify', false);

function getDatabaseUri() {
    return `mongodb://${Config.databases.mongoose.host}/${Config.databases.mongoose.dbName}`;
}

mongoose.connect(getDatabaseUri(), Config.databases.mongoose.options)
.catch(() => process.exit(1));