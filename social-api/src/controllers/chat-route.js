'use strict';
const express = require('express');
const chatRouter = express.Router();

const { mustBeUser } = require('../helpers/middleware');
const { ChatService } = require('../services/ChatService');
const chatService = new ChatService();

chatRouter.use(mustBeUser);
chatRouter.route('/').get((req, res, next) => {
	chatService
		.getAllMessage()
		.then((chat) => res.status(200).json(chat))
		.catch(res.onError);
});

module.exports = { chatRouter };
