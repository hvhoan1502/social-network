'use strict';

const express = require('express');

const { UserService } = require('../services/UserService');
const userService = new UserService();
const { uploadMiddleWare, mustBeUser } = require('../helpers/middleware');

const userRouter = express.Router();

userRouter.post('/signup', (req, res) => {
    const { name, email, password, birthday } = req.body;
    userService.signUp(name, email, password, birthday)
    .then(user => res.send({ success: true, user }))
    .catch(res.onError);
});

userRouter.post('/signin', (req, res) => {
    const { email, password } = req.body;
    userService.signIn(email, password)
    .then(user => res.send({ success: true, user }))
    .catch(res.onError);
});

userRouter.get('/check', (req, res) => {
    userService.checkSignInStatus(req.headers.token)
    .then(user => res.send({ success: true, user }))
    .catch(res.onError);
});

userRouter.use(mustBeUser);
userRouter.put('/profile', uploadMiddleWare().single('image'), (req, res) => {
    let body = req.body;
    body.avatarFile = req.file;
    userService.updateProfile(req.idUser, body)
    .then(user => res.send({ success: true, user }))
    .catch(res.onError);
})

userRouter.get('/:id', (req, res) => {
    const { id } = req.params;
    userService.getUserById(id)
    .then(user => res.send({ success: true, user }))
    .catch(res.onError);
})

module.exports = { userRouter };
