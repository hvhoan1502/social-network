'use strict';

const express = require('express');
const roomRouter = express.Router();

const { mustBeUser } = require('../helpers/middleware');
const { RoomService } = require('../services/RoomService');
const roomService = new RoomService();

roomRouter.use(mustBeUser);
roomRouter.get('/', (req, res, next) => {
    roomService.getChatRoomsByUserId(req.idUser)
        .then((rooms) => res.status(201).send({ success: true, rooms }))
        .catch(res.onError);
});

roomRouter.post('/', (req, res, next) => {
    const { friendId } = req.body;
    roomService.createChatRoom(req.idUser, friendId)
        .then((room) => res.status(201).send({ success: true, room }))
        .catch(res.onError);
});

module.exports = { roomRouter };