'use strict';

const express = require('express');
const { StoryService } = require('../services/StoryService');
const storyService = new StoryService();
const { uploadMiddleWare, mustBeUser } = require('../helpers/middleware');

const storyRouter = express.Router();

storyRouter.use(mustBeUser);
storyRouter.get('/', (req, res) => {
    storyService.getAllStory()
    .then(stories => res.send({ success: true, stories }))
    .catch(error => res.status(500).send({ success: false, message: error.message }));
});

storyRouter.get('/:authorId', (req, res) => {
    const { authorId } = req.params;
    storyService.getStoryByAuthorId(authorId)
    .then(stories => res.send({ success: true, stories }))
    .catch(error => res.status(500).send({ success: false, message: error.message }));
})

storyRouter.post('/', uploadMiddleWare().single('file'), (req, res) => {
    const file = req.file;
    storyService.createStory(req.body.content, req.idUser, file)
    .then(story => res.status(201).send({ success: true, story }))
    .catch(res.onError);
});

storyRouter.put('/:_id', (req, res) => {
    storyService.updateStory(req.params._id, req.idUser, req.body.content)
    .then(story => res.send({ success: true, story }))
    .catch(res.onError);
});

storyRouter.delete('/:_id', (req, res) => {
    storyService.removeStory(req.params._id, req.idUser)
    .then(story => res.send({ success: true, story }))
    .catch(res.onError);
});

storyRouter.post('/like/:_id', (req, res) => {
    storyService.likeStory(req.idUser, req.params._id)
    .then(story => res.send({ success: true, story }))
    .catch(res.onError);
});

storyRouter.post('/dislike/:_id', (req, res) => {
    storyService.dislikeStory(req.idUser, req.params._id)
    .then(story => res.send({ success: true, story }))
    .catch(res.onError);
});

module.exports = { storyRouter };
