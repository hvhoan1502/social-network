'use strict';

const express = require('express');

const { CommentService } = require('../services/CommentService');
const commentService = new CommentService();
const { mustBeUser } = require('../helpers/middleware');

const commentRouter = express.Router();

commentRouter.use(mustBeUser);

commentRouter.post('/', (req, res) => {
	const { content, idStory } = req.body;
	commentService
		.createComment(req.idUser, idStory, content)
		.then((comment) => res.status(201).send({ success: true, comment }))
		.catch(res.onError);
});

commentRouter.delete('/:_id', (req, res) => {
	commentService
		.removeComment(req.idUser, req.params._id)
		.then((comment) => res.send({ success: true, comment }))
		.catch(res.onError);
});

commentRouter.post('/like/:_id', (req, res) => {
	commentService
		.likeComment(req.idUser, req.params._id)
		.then((comment) => res.send({ success: true, comment }))
		.catch(res.onError);
});

commentRouter.post('/dislike/:_id', (req, res) => {
	commentService
		.dislikeComment(req.idUser, req.params._id)
		.then((comment) => res.send({ success: true, comment }))
		.catch(res.onError);
});
commentRouter.post('/search', (req, res) => {
	commentService
		.loadComment(req.body)
		.then(comments => res.send({success: true, comments}))
		.catch(res.onError);
});

module.exports = { commentRouter };
