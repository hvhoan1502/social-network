'use strict';

const router = require('express').Router();
const { storyRouter } = require("./story-route");
const { commentRouter } = require("./comment-route");
const { userRouter } = require("./user-route");
const { friendRouter } = require("./friend-route");
const { chatRouter } = require("./chat-route");
const { roomRouter } = require("./room-route");


router.use("/story", storyRouter);
router.use("/user", userRouter);
router.use("/comment", commentRouter);
router.use("/friend", friendRouter);
router.use('/chats', chatRouter);
router.use('/rooms', roomRouter);

module.exports = router;