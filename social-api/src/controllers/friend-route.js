'use strict';

const express = require('express');
const { FriendService } = require('../services/FriendService');
const friendService = new FriendService();
const { mustBeUser } = require('../helpers/middleware');

const friendRouter = express.Router();

friendRouter.use(mustBeUser);

friendRouter.get('/', (req, res) => {
    friendService.getUsers(req.idUser)
    .then(people => res.send({ success: true, people }))
    .catch(res.onError);
})

friendRouter.post('/request/:idReceiver', (req, res) => {
    friendService.sendFriendRequest(req.idUser, req.params.idReceiver)
    .then(receiver => res.send({ success: true, receiver }))
    .catch(res.onError);
});

friendRouter.post('/cancel/:idReceiver', (req, res) => {
    friendService.removeFriendRequest(req.idUser, req.params.idReceiver)
    .then(receiver => res.send({ success: true, receiver }))
    .catch(res.onError);
});

friendRouter.post('/accept/:idSender', (req, res) => {
    friendService.acceptRequest(req.idUser, req.params.idSender)
    .then(friend => res.send({ success: true, friend }))
    .catch(res.onError);
});

friendRouter.post('/decline/:idSender', (req, res) => {
    friendService.declineRequest(req.idUser, req.params.idSender)
    .then(requestor => res.send({ success: true, requestor }))
    .catch(res.onError);
});

friendRouter.post('/remove/:idFriend', (req, res) => {
    friendService.removeFriend(req.idUser, req.params.idFriend)
    .then(friend => res.send({ success: true, friend }))
    .catch(res.onError);
});

module.exports = { friendRouter };
