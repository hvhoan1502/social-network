'use strict';

const NODE_ENV = {
	LOCAL: 'local',
	DEVELOP: 'develop',
	TEST: 'test',
	UAT: 'uat',
	PROD: 'production'
};

const getEnvConfig = () => {
	const envName = process.env.NODE_ENV;
	let envFileName = '';
	switch (envName) {
		case NODE_ENV.LOCAL:
		case NODE_ENV.DEVELOP:
			envFileName = 'dev';
			break;
		case NODE_ENV.TEST:
			envFileName = 'test';
			break;
		case NODE_ENV.UAT:
			envFileName = 'uat';
			break;
		case NODE_ENV.PROD:
			envFileName = 'prod';
			break;
		default:
			envFileName = 'local'
			break;
	}

	return require(`./env/${envFileName}.json`);
}

module.exports = getEnvConfig();
