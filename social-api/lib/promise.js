/*
 * The Promise module using bluebird
 */

const promise = require('bluebird');

promise.config({
	warnings: {
		wForgottenReturn: false
	}
});

module.exports = promise;
