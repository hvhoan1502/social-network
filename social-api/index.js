'use strict';

const { server } = require("./src/app");

require("./src/helpers/connectDatabase");
require('./src/helpers/socket').socket(server);

server.listen(3000, () => console.log("Server started."));
server.on("listening", () => {
  console.log(`Listening on port:: http://localhost:3000/`);
});