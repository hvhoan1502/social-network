
WHY?
- Lý do:
    + Nhiều người thường mắc phải vấn đề này.
    + Code dài, người khác đọc sẽ khó hiểu:
        => Trực tiếp: Reviewer sẽ tốn nhiều thời gian hơn, có thể sẽ phải sửa => Tốn thời gian 2 bên.
        => Gián tiếp: Khi người khác họ tiếp tục dự án đó thì thời gian đọc và hiểu sẽ lâu hơn.
    + Ảnh hưởng tới performance, chất lượng code, đánh giá của khách hàng.

- Nguyên nhân:
    + Chính: Không có thời gian đọc lại để refactor nên những đoạn code đó sẽ được sử dụng như
một thói quen.

1. Clean code
    1. Khái niệm
        - Bjarne Stroustrup, inventor of C++: Tôi thích mã của mình phải thanh lịch và hiệu quả. Logic phải đơn giản để làm cho lỗi khó ẩn, các phụ thuộc tối thiểu để dễ bảo
        trì, xử lý lỗi hoàn thành theo một chiến lược rõ ràng và hiệu suất gần mức tối ưu để không cám dỗ mọi người làm cho mã lộn xộn với
        các tối ưu hóa không có nguyên tắc. Mã sạch làm tốt một điều.
        - Grady Booch, author of ObjectOriented Analysis and Design with
        Applications: Mã sạch rất đơn giản và trực tiếp. Mã sạch
        đọc như văn xuôi được viết tốt. Mã sạch không bao giờ
        che khuất ý định của nhà thiết kế mà thay vào đó là đầy
        trừu tượng rõ ràng và đường thẳng đơn giản
        kiểm soát.
        - “Big” Dave Thomas, founder of OTI, godfather of the
        Eclipse strategy: Mã sạch có thể được đọc và được nâng cao bởi một nhà phát triển không phải tác giả ban đầu của nó. 
        Nó có đơn vị và kiểm tra chấp nhận. Nó có ý nghĩa
        những cái tên. Nó cung cấp một cách thay vì nhiều cách để thực hiện một việc. Nó có các phụ thuộc tối thiểu, được xác định rõ
        ràng và cung cấp một API rõ ràng và tối thiểu. Code phải thành thạo vì tùy thuộc vào ngôn ngữ, không phải tất cả các thông tin cần 
        thiết đều có thể được diễn đạt rõ ràng chỉ trong code.
    1.1. Giới thiệu
        - Công việc chính của một lập trình viên chính là code, là lập trình. Vậy muốn trở thành một lập trình viên tốt hơn thì đơn giản chỉ là code tốt hơn, phải vậy không?
        Khi viết một chương trình hay một function, đầu tiên là lên ý tưởng, và tiếp theo là code và code cho đến khi nó chạy được, và sau đó ... à không có sau đó nữa.
        Đa số chúng ta khi bắt đầu theo nghề đều như vậy, và đôi khi việc "làm cho xong" trở thành một thói quen khó bỏ. Và hậu quả để lại là một nùi code smell, gây khó khăn
        cho những người maintain, hay tiếp tục phần việc của bạn, và đôi khi người đó lại chính là bạn. Hãy thử nghĩ đến cảm giác khi vừa code vừa "nguyền rủa" người để lại 
        những dòng code smell này, và người đó lại chính là bạn. Mình tin là đa số trong chung ta đã trải qua cảm giác này, không dễ chịu chút nào phải không?
        Và đây là lúc để thay đổi lối code, hãy code clean hơn, trở nên chuyên nghiệp hơn.
        - Lý do tại sao bạn phải quan tâm đến code của mình là code của bạn sẽ mô tả quá trình suy nghĩ của bạn cho người khác. Đó là lý do tại sao bạn 
        phải bắt đầu suy nghĩ về việc làm cho code của bạn thanh lịch hơn, đơn giản và dễ đọc hơn.
    1.2. Đặc điểm
        - Source Code phải thanh lịch: Hãy hình dung đến 2 chữ "thanh lịch", nó phải sạch sẽ, cảm thấy hứng thú khi đọc. Giống như khi bạn chăm chú đọc một bài văn hay vậy.
        - Code phải có tâm điểm (focus): Mỗi fucntion, mỗi class, mỗi module chỉ thực hiện một chức năng duy nhất, không bị phân tán, bởi các context xung quanh.
        - Clean code thì được trau chuốt, người code ra nó đã dành thời gian đển giữ cho nó đơn giản và có trật tự nhất có thể.
        - Chạy tốt trên tất cả các case : Đôi khi các function được tạo ra chỉ chạy tốt trên các case bình thường, còn các case "quái dị" thì xảy ra lỗi.
        - Tối ưu số lượng các class, method, function ...
    1.3. Quy cách
        - Đặt tên có ý nghĩa: Chọn một cái tên tốt cho source code có lẽ sẽ tốn thời gian, nhưng nó còn đỡ hơn nhiều so với việc phải đọc toàn bộ code mới có thể hiểu được 
        nó có chức năng gì phải không nào ? Tên của một biến, hàm, class sẽ trả lời được các câu hỏi quan trọng như "nó làm gì? ", "tại sao nó tồn tại? " và "cách nó được sử 
        dụng? ".
            + Class name: các class và các object nên có tên là danh từ hoặc cụm danh từ như Customer, Account, Client,... Tránh cách từ như Manager, Processor, 
            Data hoặc chứa thông tin trong tên của một class. Một tên class không nên là một động từ.
            + Method name: nên được bắt đầu bằng một động từ hoặc một cụm động từ như postPayment(), deleteUser() , hoặc saveScore(). Các method dùng để access vào
            các trường trong class thì nên được tên theo tên trường đó và đước trước là set/get. Ví dụ Class User có trường là name. Thì các method để đọc và ghi dữ liệu 
            cho 2 trường này sẽ là setName(...) , getName().
            + Pick one word per concept : Chọn một từ cho một function trừu tượng gắn với nó (abstract funtion). Hơi khó hiểu một chút. Chẳng hạn khi một class kế thường nhiều 
            các interface, mà trong các interface đó lại có nhiều các abstract funtion thì làm sao mà chúng ta có thể biết được method nào là của interface nào mà triển khai 
            cho đúng logic. Và giải pháp ở đây là chọn một từ (danh từ) giống với class/interface chứa abstract funtion đó. Ví dụ interface : ActionPostListener thì abstract 
            fun của nó là savePost(), sharePost, deletePost()...
    1.5. Các ví dụ về code đẹp trong JS

2. Performance
    1.1. Là gì
    1.2. Tại sao phải quan tâm tới performance
    1.3. Lợi ích.
    1.5. Các lỗi thường gặp và cách khắc phục trong JS.

Document Links:
1. https://github.com/ryanmcdermott/clean-code-javascript
2. https://dev.to/redbossrabbit/10-clean-code-examples-javascript-37kj
3. https://www.w3schools.com/js/js_performance.asp