let students = [
    { name: 'Nhat', point: 4 },
    { name: 'Minh', point: 8 },
    { name: ' Thuan', point: 9 },
    { name = 'Nam', point: 10 }
];

// Not good
for (let i = 0; i < students.length; i++) {
    students[i].point >= 8 && hsg.push(students[i]);
}

const excellentStudents = users.filter(({ point }) => point >= 8).map(({ name }) => name);

// Good
const studentNumber = students.length;
for (let i = 0; i < studentNumber; i++) {
    students[i].point >= 8 && hsg.push(students[i]);
}


// The single loop
const names = [];
users.forEach(({ age, name }) => {
  age >= 18 && names.push(name);
});

// The reduce method
const valid = users.reduce((acc, { age, name }) => {
  return (age >= 18) ? [...acc, name] : acc;
}, []);



const users = [{
    name: 'Ronaldo',
    age: 23
}, {
    name: 'Messi',
    age: 14
}, {
    name: 'Anoystick',
    age: 22
}]
// Bài toán tìm tên của các cầu thủ đó độ tuổi trên 18. bài toán rất đơn giản và rất bình thường nhưng nó thật sự khác biệt khi xử lý hàng triệu record đó các bạn. 

// Code1:
const valid = users.filter(({ age }) => age >= 18).map(({ name }) => name);
// Quan trọng không bỏ qua phần này.
// Rất đẹp và tường minh. Code này dùng rất điệu nghệ và có vẻ đọc sách vở rất nhiều, các hàm map(), filter() được sử dụng ở đây. Nó sử dụng hai phương thức ES5, map(), filter(), và các arrow function ES6 và object de-structuring. Nó cũng viết và dễ đọc. Trông thật tuyệt! 

// Nhưng nó không phải là vấn đề, nhưng các bạn thử hình dung nếu chúng ta có users với 100.000 object thì performance nó như thế nào? 

// Phân tích ví dụ : Như tôi nói nếu có 100.000 object trong array user thì bạn sẽ phải filter() 100.000 lần mới có thể lọc ra được những object phù hợp hơn 18 tuổi. Ví dụ là được 50.000 pass qua vậy chúng ta phải chạy thêm 50.000 lần nữa để có thể get được name của các object. Vậy chương trình của bạn sẽ thực hiện 150.000 lần lặp thay vì chỉ có 100.000 lặp để lấy ra kết quả. Vậy hiệu suất nó như thế nào?

// Code2: 

// Thực tế có hai cách, một sử dụng single loop hoặc sử dụng reduce.

// The single loop
const names = [];
users.forEach(({ age, name }) => {
  age >= 18 && names.push(name);
});

// The reduce method
const valid = users.reduce((acc, { age, name }) => {
  return (age >= 18) ? [...acc, name] : acc;
}, []);
// Cú pháp reduce có thể trông có thể lạ lẫm hơn, nhưng nó thực hiện chính xác giống như single loop, cách chúng ta thường làm trước các phương thức mảng ES5. 
// Qua các cách làm ở ví dụ trên các bạn cũng đã hình dung được mình đang ở vị trí nào?
// Và ở bất kỳ một vị trí nào các bạn cũng nên đặt performance lên đầu tiên cho các dòng code của các bạn.
// Không hẳn thông qua các đoạn mã để dánh giá về performance mà còn rất nhiều yếu tố nữa cụ thể như