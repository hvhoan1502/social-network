WHY?
- Lý do:
    + Nhiều người thường mắc phải vấn đề này.
    + Code dài, người khác đọc sẽ khó hiểu:
        => Trực tiếp: Reviewer sẽ tốn nhiều thời gian hơn, có thể sẽ phải sửa => Tốn thời gian 2 bên, Ảnh hưởng tới performance, chất lượng code, đánh giá của khách hàng.
        => Gián tiếp: Khi người khác họ tiếp tục dự án đó thì thời gian đọc và hiểu sẽ lâu hơn.
- Nguyên nhân:
    + Chính: Không có thời gian đọc lại để refactor nên những đoạn code đó sẽ được sử dụng như một thói quen.

*** Clean code Intro
- Công việc chính của một lập trình viên chính là code, là lập trình. Vậy muốn trở thành một lập trình viên tốt hơn thì đơn giản chỉ là code tốt hơn, phải vậy không?
        Khi viết một chương trình hay một function, đầu tiên là lên ý tưởng, và tiếp theo là code và code cho đến khi nó chạy được, và sau đó ... à không có sau đó nữa.
        Đa số chúng ta khi bắt đầu theo nghề đều như vậy, và đôi khi việc "làm cho xong" trở thành một thói quen khó bỏ. Và hậu quả để lại là một nùi code smell, gây khó khăn
        cho những người maintain, hay tiếp tục phần việc của bạn, và đôi khi người đó lại chính là bạn. Hãy thử nghĩ đến cảm giác khi vừa code vừa "nguyền rủa" người để lại 
        những dòng code smell này, và người đó lại chính là bạn. Mình tin là đa số trong chung ta đã trải qua cảm giác này, không dễ chịu chút nào phải không?
        Và đây là lúc để thay đổi lối code, hãy code clean hơn, trở nên chuyên nghiệp hơn.
        - Lý do tại sao bạn phải quan tâm đến code của mình là code của bạn sẽ mô tả quá trình suy nghĩ của bạn cho người khác. Đó là lý do tại sao bạn 
        phải bắt đầu suy nghĩ về việc làm cho code của bạn thanh lịch hơn, đơn giản và dễ đọc hơn.
*** Nét đặc trưng
 - Source Code phải thanh lịch: Hãy hình dung đến 2 chữ "thanh lịch", nó phải sạch sẽ, cảm thấy hứng thú khi đọc. Giống như khi bạn chăm chú đọc một bài văn hay vậy.
        - Code phải có tâm điểm (focus): Mỗi fucntion, mỗi class, mỗi module chỉ thực hiện một chức năng duy nhất, không bị phân tán, bởi các context xung quanh.
        - Clean code thì được trau chuốt, người code ra nó đã dành thời gian đển giữ cho nó đơn giản và có trật tự nhất có thể.
        - Chạy tốt trên tất cả các case : Đôi khi các function được tạo ra chỉ chạy tốt trên các case bình thường, còn các case "quái dị" thì xảy ra lỗi.
        - Tối ưu số lượng các class, method, function ...

Use meaningful and pronounceable variable names:  Sử dụng tên biến có nghĩa và dễ phát âm.
Use the same vocabulary for the same type of variable: Sử dụng cùng từ vựng cho cùng loại biến.
Use searchable names: Sử dụng các tên có thể tìm kiếm được
Use explanatory variables:  Sử dụng những biến có thể giải thích được
Avoid Mental Mapping: Tránh hại não người khác
Don’t add unneeded context:  Đừng thêm những ngữ cảnh không cần thiết
Use default arguments instead of short circuiting or conditionals: Sử dụng những tham số mặc định thay vì kiểm tra các điều kiện lòng vòng




