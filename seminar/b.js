
// Not good
Array.prototype.sum = function (sumArray = []) {
    return sumArray.reduce((a, b) => a + b, 0)
}

const total = Array.prototype.sum([1, 2, 3, 4]);
// Output: total = 10


// class SuperArray extends Array {
//     static sum(sumArray = []) {
//         return sumArray.reduce((a, b) => a + b, 0);
//     }
// }

// const total = SuperArray.sum([1, 2, 3, 4]);
// Output: total = 10;

function getAddressDetail() {
	return ['Quan 1', 'Ho Chi Minh City', '70000'];
}

const [district, city, zipCode] = getAddressDetail();
console.log(district);
console.log(city);
console.log(zipCode);

