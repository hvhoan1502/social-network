let students = [
    {
        name: 'Nhat',
        point: 4
    },
    {
        name: 'Minh',
        point: 8
    },
    {
        name: ' Thuan',
        point: 9
    },
    {
        name = 'Nam',
        point: 10
    }
];

// 1 For
let excellentStudents = [];
let hsk = [];
let hstb = [];
let hsy = [];

// instead of
for (let i = 0; i < students.length; i++) {
    if (students[i].point >= 8) {
        excellentStudents.push(students[i]);
    }
}


// Use
for (let i = 0; i < students.length; i++) {
    students[i].point >= 8 && excellentStudents.push(students[i]);
}

// OR
const excellentStudents = studies.filter((student) => student.point >= 8);

// 2. If else

// instead of
math.round(math.random*50)
// use
~~(math.random*50)
// You can also use the “~~” operator to convert anything into a number value.
// Example snippet:
~~('whitedress') // returns 0
~~(NaN) // returns 0

var array = ['a', 'b', 'c', 'd', 'e'];
console.log(array.length); 
// Returns the length as 5


array.length = 4;
console.log(array.length); 
// returns the length as 4
console.log(array); 
// returns “a,b,c,d”

array.length = 0;
console.log(array.length); 
// returns the length as 0
console.log(array);

var list1 = [a, b, c, d, e];
var list2 = [f, g, h, i, j];
console.log(list1.push.apply(list1, list2));


const cars = ['Opel', 'Bugatti', 'Opel', 'Ferrari', 'Ferrari', 'Opel'];
var unrepeated_cars = [...new Set(cars)];
// Log: Set { 'Opel', 'Bugatti', 'Ferrari' }
console.log(unrepeated_cars); 
// Log: [ 'Opel', 'Bugatti', 'Ferrari' ]


const data = {}
// instead of
if(data && data.box && data.box.name === 'test') {
    // do something
}

// use
if(data?.box?.name === 'test') {
    // do something
}