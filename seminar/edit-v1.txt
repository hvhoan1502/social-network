Change Clean Code -> Code Beautify Tips
1.2 Remove
1.3
 - Use the same vocabulary for the same type of variable: Check again
 - Use explanatory variables: change cityZipCodeRegex -> split function -> X
 - Avoid Mental Mapping: Example move to 'Use meaningful and pronounceable variable names. Find other example
 - Use default arguments instead... : change breweryName -> name -> Done
 - Function arguments: Add more parametter in bad code. Change Object properties -> object name  -> Done
 - Functions should do one thing: Add more child function to emailClients in bad code, break function must use on another palace -> Done
 - Function names should say what they do: Example move to ' Use meaningful and ...', addMonthToDate? -> Done
 - Don't use flags as function parameters: log the result.
 - Encapsulate conditionals: log thẻ result.
 - Don't write to global functions: Check agian
 - Avoid type-checking: Need change
 - Change " -> '
 - Change Arrow Function -> basic
 - Change tab code from 2 to 4

