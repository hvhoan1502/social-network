// 1.
const number = 10.585566;
Math.round(number);
// Output: 11
~~number;
// Output: 10


// 2.
const array = [1, 2, 3, 4, 5, 6, 7, 8, 9]
// array.length = 9
array.length = 2;
// array = [1, 2]

// 3.
let array1 = [1, 2, 3, 4, 5, 6, 7]
const array2 = [8, 9]
array1 = array1.concat(array2);
// Output: [1, 2, 3, 4, 5, 6, 7, 8, 9]
array1.push.apply(array1, array2);
// Output: [1, 2, 3, 4, 5, 6, 7, 8, 9]

// 4

const people = {
    name: 'Tèo',
    age: 18,
    address: {
        street: '',
    }
}

// instead of
if (people && people.address && people.address.street) {
    // do something
}



