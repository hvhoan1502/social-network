// console.log(typeof [1, 2]);
// console.log(typeof null);
// console.log(typeof 'foo');
// console.log(typeof new String('foo'));
// console.log('foo' instanceof String);
// console.log(new String('foo') instanceof String);
// console.log(new String('foo') instanceof Object);

typeof null;
// Output: object
typeof undefined;
// Output: undefined
typeof NaN;
// Output: number
typeof 5;
// Output: number
typeof [1, 2];
// Output: object
typeof 'Seminar';
// Output: string
typeof new String('Seminar');
// Output: object
typeof ('Seminar' instanceof String);
// Output: false

// Not good
function sum(number1, number2) {
	if (typeof number1 === 'string') {
		number1 === parseInt(number1);
	}
	if (typeof number2 === 'string') {
		number2 === parseInt(number2);
	}

	return number1 + number2;
}

const number1 = new String('1');
const number2 = 2;
sum(number1, number2);
// Output: 12

// Good
// function sum(number1, number2) {
// 	return Number(number1) + Number(number2);
// }
// const number1 = new String('1');
// const number2 = 2;
// sum(number1, number2);
// Output: 3

const students = ['Tí Sún', 'Sửu Ẹo', 'Dần Béo', 'Cả Mẹo', 'Mùi Mập'];
// Output: students.length = 5;

// Solution
students.pop();
// Output: students = ['Tí Sún', 'Sửu Ẹo', 'Dần Béo', 'Cả Mẹo'];

students = [];

//Better
students.length = 4;
// Output: students = ['Tí Sún', 'Sửu Ẹo', 'Dần Béo', 'Cả Mẹo'];

students.length = 0;
// Output: students = [];

// Good
function newRequestModule(url) {
	// to do
}

const req = newRequestModule('www.inventory-awesome.io');

// Not good
let a = 0; // user ages
let w = 0; // user weight
let h = 0; // user height

class UserInfo {}

// Good
let userAge = 0;
let userWeight = 0;
let userHeight = 0;

class Users {}

const timeOut = (t) => {
	return new Promise((resolve, reject) => {
		setTimeout(() => {
			resolve(`Completed in ${t}`);
		}, t);
	});
};

const durations = [1000, 2000, 3000];

const promises = [];

durations.map((duration) => {
	promises.push(timeOut(duration));
});

console.log(promises);
// Output:  [ Promise { "pending" }, Promise { "pending" } ...]

Promise.all(promises).then((response) => console.log(response));
// Output: ["Completed in 1000", "Completed in 2000", "Completed in 3000"]

