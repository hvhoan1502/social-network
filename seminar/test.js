// Not good
const fullName = firstName + ' ' + lastName;
document.getElementById('demo').innerHTML = fullName;

// Good
document.getElementById('demo').innerHTML = firstName + ' ' + lastName;

// ------------------------------------------------------------------------

// Not good
function getAddressDetail() {
	return ['Quan 1', 'Ho Chi Minh City', '70000'];
}

saveCityZipCode(getAddressDetail[1], getAddressDetail[2]);

// Good
function getAddressDetail() {
	return ['Quan 1', 'Ho Chi Minh City', '70000'];
}

const [district, city, zipCode] = getAddressDetail();
// Output: district = 'Quan 1', city = 'Ho Chi Minh City', zipCode = 70000;
saveCityZipCode(city, zipCode);

// ------------------------------------------------------------------------

// Not good
function createUser(userInfo, role) {
	role = role || 'guest';
	// Output: role = 'guest'
	// to do
}

// Good
const DEFAULT_ROLE = 'guest';
function createUser(userInfo, role = DEFAULT_ROLE) {
	// Output: role = 'guest'
	// to do
}

// ------------------------------------------------------------------------

// Not good
function createUser(userName, email, password, phoneNumber) {
	// to do
}

createUser('hoanhv', 'hoanhv@icd-vn.com', 'seminar', '19001000');

// Good
function createUser(userInfo) {
	// to do
}

createUser({
	userName: 'hvhoan',
	email: 'hoanhv@icd-vn.com',
	password: 'seminar',
	phoneNumber: '19001000'
});

// ------------------------------------------------------------------------

// Not good
function emailClients(clients) {
	clients.forEach(function (client) {
		const clientRecord = database.lookup(client);
		if (clientRecord.isActive()) {
			email(client);
		}
	});
}

// Good
function emailActiveClients(clients) {
	clients.filter(isActiveClient).forEach(email);
}

function isActiveClient(client) {
	const clientRecord = database.lookup(client);
	return clientRecord.isActive();
}

// ------------------------------------------------------------------------

// ------------------------------------------------------------------------

// Not good
function createMenu(config) {
	config.title = config.title || 'Foo';
	config.body = config.body || 'Bar 1';
	config.buttonText = config.buttonText || 'Baz';
	config.cancellable = config.cancellable != undefined ? config.cancellable : true;

	return config;
}

const menuConfig = {
	title: null,
	body: 'Bar',
	buttonText: null,
	cancellable: false
};
createMenu(menuConfig);
// Output: { title: 'Foo', body: 'Bar', buttonText: 'Baz', cancellable: false }

// Good
function createMenu(config) {
	const defaultConfig = {
		title: 'Foo',
		body: 'Bar 1',
		buttonText: 'Baz',
		cancellable: true
	};

	return Object.assign(defaultConfig, config);
}

const menuConfig = {
	body: 'Bar',
	cancellable: false
};
createMenu(menuConfig);
// Output: { title: 'Foo', body: 'Bar', buttonText: 'Baz', cancellable: false }

// ------------------------------------------------------------------------

// Not good
const yyyymmdstr = moment().format('YYYY/MM/DDD');
// or
const date = moment().format('YYYY/MM/DDD');

// Good
const currentDate = moment().format('YYYY/MM/DD');

// ------------------------------------------------------------------------

// Not good
students.forEach(function (s) {
	// to do
});

// Good
students.forEach(function (student) {
	// to do
});

// ------------------------------------------------------------------------

// Not good
function addToDate(date, month) {
	// to do
}

const date = new Date();
addToDate(date, 1);

// Good
function addMonthToDate(month, date) {
	// to do
}

const currentDate = new Date();
addMonthToDate(currentDate, 1);

// Not good
function getData() {
	const listCustomer = [];
	const data = serviceRepository.getPlainCustomers();
	data.forEach(function (item) {
		if (item != null) {
			listCustomer.push(item);
		}
	});

	return listCustomer;
}

// Good
function getValidCustomers() {
	const validCustomers = [];
	const plainCustomers = serviceRepository.getPlainCustomers();
	plainCustomers.forEach(function (customer) {
		customer && validCustomers.push(customer);
	});

	return validCustomers;
}

// ------------------------------------------------------------------------

// Not good
const car = {
	carMake: 'Lamborghini',
	carModel: 'Urus',
	carWeight: 2000,
	carColor: 'yellow'
};

// Good
const car = {
	make: 'BMW',
	model: 'X6',
	weight: 2200,
	color: 'back'
};

// Not good
class Person {
	constructor() {
		this.personName = 'Person';
		this.personAge = 18;
	}
}

// Good
class Person {
	constructor() {
		(this.name = 'Person'), (this.age = 18);
	}
}

// ------------------------------------------------------------------------

// Not good
function showDevelopers(developers) {
	developers.forEach(function (developer) {
		const expectedSalary = developer.calculateExpectedSalary();
		const experience = developer.getExperience();
		const linkedInLink = developer.getLinkedInLink();

		render({ expectedSalary, experience, linkedInLink });
	});
}

function showManagers(managers) {
	managers.forEach(function (manager) {
		const expectedSalary = manager.calculateExpectedSalary();
		const experience = manager.getExperience();
		const portfolio = manager.getMBAProjects();

		render({ expectedSalary, experience, portfolio });
	});
}

// Good
const MANAGER = 'manager';
const DEVELOPER = 'developer';

function showEmployees(employees, type) {
	employees.forEach(function (employee) {
		const expectedSalary = employee.calculateExpectedSalary();
		const experience = employee.getExperience();
		let data = { expectedSalary, experience };

		switch (type) {
			case DEVELOPER:
				data.linkedInLink = employee.getLinkedInLink();
				break;
			case MANAGER:
				data.portfolio = employee.getMBAProjects();
				break;
			default:
				true;
		}

		render(data);
	});
}

// ------------------------------------------------------------------------

// Not good
function createFile(name, temp) {
	if (temp) {
		fs.create(`./temp/${name}`);
	} else {
		fs.create(name);
	}
}

// Good
function createFile(name) {
	fs.create(name);
}

function createTempFile(name) {
	createFile(`./temp/${name}`);
}

// ------------------------------------------------------------------------

// Not good
const fsmInstance = { state: 'fetching' };
const listNode = { type: 'example' };

if (fsmInstance.state === 'fetching' && Object.keys(listNode).length && listNode.type === 'example') {
	console.log('Passed');
}
// Log: Passed

// Good
function shouldShowSpinner(fsm, listNode) {
	return fsm.state === 'fetching' && Object.keys(listNode).length && listNode.type === 'example';
}

if (shouldShowSpinner(fsmInstance, listNode)) {
	console.log('Passed');
}
// Log: Passed

// ------------------------------------------------------------------------

// Not good
getUserDetail(userId);
// OR
getUserData(userId);
// OR
getUserRecord(userId);

// Good
getUser(userId);

// ------------------------------------------------------------------------

// Not good
function emailClients(clientIds) {
	clientIds.forEach(function (clientId) {
		const clientRecord = clientRepository.findById(clientId);
		if (clientRecord && clientRecord.isActive()) {
			email(client);
		}
	});
}

// Good
function emailActiveClients(clientIds) {
	clientIds.filter(isActiveClient).forEach(email);
}

function isActiveClient(clientId) {
	const clientRecord = clientRepository.findById(clientId);
	return clientRecord && clientRecord.isActive();
}

// ------------------------------------------------------------------------

// Not good
typeof [1, 2];
// Output: object
typeof null;
// Output: object
typeof 'Seminar';
// Output: string
typeof new String('Seminar');
// Output: object
typeof ('Seminar' instanceof String);
// Output: false
typeof (new String('Seminar') instanceof String);
// Output: true
typeof (new String('Seminar') instanceof Object);
// Output: true

