import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl, FormBuilder } from '@angular/forms';

import { UserService } from '../../../core/services/user.service';

@Component({
  selector: 'app-sign-up-page',
  templateUrl: './sign-up-page.component.html',
  styleUrls: ['./sign-up-page.component.css']
})
export class SignUpComponent implements OnInit {
  formSignUp: FormGroup;
  constructor(private fb: FormBuilder, private userService: UserService) {
    this.formSignUp = this.fb.group({
      firstName:  ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      email: ['', [Validators.required]],
      password: ['', [Validators.required]],
      birthDay: ['', Validators.required]
    });
  }

  ngOnInit() {
  }

  signUp() {
    const { firstName, lastName, email, password, birthDay } = this.formSignUp.value;
    this.userService.singUp(`${firstName} ${lastName}`, email, password, birthDay);
  }
}
