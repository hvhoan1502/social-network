import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { StoryService } from 'src/app/core/services/story.service';
import { Store } from '@ngrx/store';
import { AppState, Story, UserInfo } from 'src/app/core/interfaces';
import { UserService } from 'src/app/core/services/user.service';

@Component({
  selector: 'app-story-list',
  templateUrl: './story-list.component.html',
  styleUrls: ['./story-list.component.css'],
})
export class StoriesComponent implements OnInit {
  imageURL: string;
  uploadForm: FormGroup;
  stories: Story[] = [];
  imgEncode: any = '';
  files: File[] = [];
  isAddStory: Boolean = false;
  formData = new FormData();
  idUser: string = '';

  constructor(
    private fb: FormBuilder,
    private storyService: StoryService,
    private store: Store<AppState>,
    private userService: UserService
  ) {
    // Reactive Form
    (this.uploadForm = this.fb.group({
      storyImage: [null],
      content: ['', [Validators.required]],
    })),
      (this.imageURL = '');
  }

  async ngOnInit(): Promise<void> {
    await this.userService.check();
    await this.store
      .select('stories')
      .subscribe((stories) => (this.stories = stories));
    this.store
      .select('user')
      .subscribe((user: UserInfo) => (this.idUser = user._id));
  }

  // Image Preview
  showPreview(event: any) {
    const file = event.target.files[0];
    this.uploadForm.patchValue({
      storyImage: file,
    });
    this.uploadForm.get('storyImage')?.updateValueAndValidity();

    // File Preview
    const reader = new FileReader();
    reader.onload = () => {
      this.imageURL = reader.result as string;
    };
    reader.readAsDataURL(file);
  }

  // Submit Form
  createStory() {
    const { content } = this.uploadForm.value;
    this.formData.append('content', content);
    this.storyService.createStory(this.formData).then((res) => {
      this.files = [];
      this.isAddStory = false;
      this.uploadForm.controls['content'].setValue('');
    });
  }

  onSelect(event: any) {
    this.files.push(...event.addedFiles);

    this.formData.append('file', this.files[this.files.length - 1]);
  }

  onRemove(event: any) {
    this.files.splice(this.files.indexOf(event), 1);
  }

  addStory(event: any) {
    this.isAddStory = true;
  }

  closeSection(event: any) {
    this.isAddStory = false;
  }
}
