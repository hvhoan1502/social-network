import { CommentService } from 'src/app/core/services/comment.service';
import { Component, Input, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/core/interfaces';
import { CATEGORY_PAGINATION, DEFAULT_PAGE_NUMBER } from "src/app/shared/common/constants";

@Component({
  selector: 'app-comment-list',
  templateUrl: './comment-list.component.html',
  styleUrls: ['./comment-list.component.css'],
})
export class CommentListComponent implements OnInit {
  @Input() story: any;
  @Input() idUser: string = '';
  txtComment: string = '';
  pageIndex: number = 1;
  pageNumber: number = DEFAULT_PAGE_NUMBER;

  constructor(private commentService: CommentService, private store: Store<AppState>) {}

  ngOnInit(): void {
    this.store.select('paginate').subscribe(paginateInfo => {
      const commentPagination = paginateInfo[CATEGORY_PAGINATION.Comment].filter((item: any) => item.storyId === this.story._id);
      if (commentPagination.length) {
        this.pageIndex = commentPagination[0].pageIndex;
        this.pageNumber = commentPagination[0].pageNumber;
      }
    });
  }

  createComment() {
    this.commentService.createComment(this.story._id, this.txtComment);
    this.txtComment = '';
  }

  loadComments(storyId: string) {
    this.commentService.loadComments(storyId, this.pageIndex, this.pageNumber);
    this.store.dispatch({ 
      type: 'LOAD_COMMENT_PAGINATION',
      category: CATEGORY_PAGINATION.Comment,
      pageIndex: this.pageIndex,
      pageNumber: this.pageNumber,
      _id: this.story._id
    })
  }
}
