import { Component, Input, OnInit } from '@angular/core';
import { UserInfo } from 'src/app/core/interfaces';
import { StoryService } from 'src/app/core/services/story.service';

@Component({
  selector: 'app-story-item',
  templateUrl: './story-item.component.html',
  styleUrls: ['./story-item.component.css']
})
export class StoryComponent implements OnInit {
  @Input() story: any;
  @Input() idUser: string = '';
  today: Date = new Date();
  isShowComment: boolean = false;

  constructor(private storyService: StoryService){}

  ngOnInit(): void {
  }

  get liked(): boolean {
    return this.story.fans.filter((fan: UserInfo) => this.idUser == fan._id).length;
  }

  get commented(): boolean {
    return this.story.commentNumber;
  }

  toggleLike() {
    if (!this.liked) return this.storyService.likeStory(this.story._id, this.idUser);
    this.storyService.dislikeStory(this.story._id, this.idUser);
  }

  removeStory() {
    return this.storyService.removeStory(this.story._id);
  }

  showComment() {
    this.isShowComment = !this.isShowComment;
  }
}
