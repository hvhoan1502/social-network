import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { CommentService } from 'src/app/core/services/comment.service';

@Component({
  selector: 'app-comment',
  templateUrl: './comment-item.component.html',
  styleUrls: ['./comment-item.component.css']
})
export class CommentComponent implements OnInit {
  @Input() comment: any;
  @Input() idUser: string = '';

  constructor(private commentService: CommentService,
              private router: Router
            ) { }

  ngOnInit(): void {}

  get liked() {
    return this.comment.fans.some((fan: string) => fan == this.idUser);
  }

  likeComment(_id: string) {
    this.commentService.likeComment(_id);
  }

  navigateToProfile() {
    this.router.navigate([`/profile`], { queryParams: { id: this.comment.author._id }})
  }

  dislikeComment(_id: string) {
    this.commentService.dislikeComment(_id);
  }

}
