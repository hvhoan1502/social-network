import { Component, OnInit } from '@angular/core';
import { StoryService } from '../../../core/services/story.service';
@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {
  constructor(private storyService: StoryService) {}

  ngOnInit(): void {
    this.storyService.getAllStory();
  }
}
