import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { AppState, User } from 'src/app/core/interfaces';
import { UserService } from 'src/app/core/services/user.service';
import { defaultUser } from 'src/app/shared/common/defaultValue';

@Component({
  selector: 'app-user-from',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class ProfileFormComponent implements OnInit {
  user: User = defaultUser;
  profileGroup: FormGroup;
  file: any;

  constructor( private store: Store<AppState>,
                private fb: FormBuilder,
                private userService: UserService
              ) {
    this.profileGroup = this.fb.group({
      newPassword: [''],
      birthDay: [null, [Validators.required]],
      description: ['', [Validators.required]]
    })
  }

  async ngOnInit(): Promise<void> {
    await this.userService.check();
    this.store.select('user').subscribe(user => this.user = user);
  }

  onChangeImage(event: any) {
    this.file = event.target.files.length ? event.target.files[0] : null;
  }

  updateProfile() {
    const { newPassword, birthDay, description } = this.profileGroup.value;
    const formData = new FormData();
    formData.append('image', this.file);
    formData.append('newPassword', newPassword);
    formData.append('birthday', birthDay);
    formData.append('description', description);

    return this.userService.updateProfile(formData);
  }

}
