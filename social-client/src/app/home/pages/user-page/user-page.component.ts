
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { ActivatedRoute } from '@angular/router';

import { UserService } from '../../../core/services/user.service';
import { FriendService } from 'src/app/core/services/friend.service';
import { AppState, User } from 'src/app/core/interfaces';
import { defaultUser } from 'src/app/shared/common/defaultValue';

@Component({
  selector: 'app-user-page',
  templateUrl: './user-page.component.html',
  styleUrls: ['./user-page.component.css']
})
export class ProfileComponent implements OnInit {
  activeUser: User = defaultUser;
  userLogin: any;
  constructor(private userService: UserService,
              private router: ActivatedRoute,
              private store: Store<AppState>,
              private friendService: FriendService
            ) { }

  ngOnInit(): void {
    this.router.queryParams.subscribe(async params => {
      const userId: string = params['id'];
      await this.userService.check();
      this.friendService.getPeople();
      await this.store.select('user').subscribe(userStored => this.userLogin = userStored);
      
      if (!userId) this.activeUser = this.userLogin;
      else {
        await this.userService.getUserById(userId);
        this.store.select('friendInfo').subscribe(friendInfo => this.activeUser = friendInfo);
      }
    })
  }
}
