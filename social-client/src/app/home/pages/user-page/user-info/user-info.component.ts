import { Component, Input, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

import { UserService } from '../../../../core/services/user.service';
import { FriendService } from 'src/app/core/services/friend.service';
import { AppState, People, UserInfo } from 'src/app/core/interfaces';
import { defaultPeople } from 'src/app/shared/common/defaultValue';
import { ChatService } from 'src/app/core/services/chat.service';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.css']
})
export class InfoComponent implements OnInit {
  @Input() user: any = {};
  @Input() idLogin: string  = '';
  isLoginUser: boolean = false;
  people: People = defaultPeople;

  constructor(private userService: UserService,
              private friendService: FriendService,
              private store: Store<AppState>,
              private chatService: ChatService) {}

  ngOnInit(): void {
    this.store.select('people').subscribe((p: any) => this.people = p);
  }

  get displayAddFriendBtn(): string {
    const isLoggedInUser = this.user?._id == this.idLogin;
    const isFriend = this.people.friends.filter((userId: UserInfo)  => userId._id == this.user._id).length;
    const isSentRequest = this.people.sentRequests.filter((userId: UserInfo)  => userId._id == this.user?._id).length;
    const isComingRequest = this.people.incomingRequests.filter((userId: UserInfo)  => userId._id == this.user._id).length;

    if (isLoggedInUser || isFriend || isSentRequest || isComingRequest) {
      return 'none';
    }

    return 'block';
  }

  get displayRemoveFriendBtn(): string {
    const isLoggedInUser = this.user?._id == this.idLogin;
    const isFriend = this.people.friends.filter((userId: UserInfo)  => userId._id == this.user._id).length;
    return (!isLoggedInUser && isFriend) ? 'block' : 'none';
  }

  get displayFriendRequestBtn(): string {
    const isComingRequest = this.people.incomingRequests.filter((userId: UserInfo)  => userId._id == this.user._id).length;
    return isComingRequest ? 'block' : 'none';
  }

  get cancelSentRequestBtn(): string {
    const isSentRequest = this.people.sentRequests.filter((userId: UserInfo)  => userId._id == this.user?._id).length;
    return isSentRequest ? 'block' : 'none';
  }

  logOut() {
    this.userService.logOut();
  }

  addFriend() {
    this.friendService.addFriend(this.user);
  }

  removeFriend() {
    this.friendService.removeFriend(this.user);
  }

  cancelSentRequest() {
    this.friendService.cancelFriendRequest(this.user);
  }

  declineRequest() {
    this.friendService.declineFriendRequest(this.user);
  }

  acceptRequest() {
    this.friendService.acceptFriendRequest(this.user);
  }

  createChatRoom() {
    this.chatService.createChatRoom(this.user._id);
  }
}
