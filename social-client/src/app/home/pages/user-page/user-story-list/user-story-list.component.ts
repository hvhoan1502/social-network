import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-story-list',
  templateUrl: './user-story-list.component.html',
  styleUrls: ['./user-story-list.component.css']
})

export class MyStoriesComponent implements OnInit {
  @Input() stories: any;
  constructor() { }

  ngOnInit(): void {}
}
