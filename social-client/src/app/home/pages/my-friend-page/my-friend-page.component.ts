import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { FriendService } from 'src/app/core/services/friend.service';
import { AppState } from 'src/app/core/interfaces';

@Component({
  selector: 'app-my-friend-page',
  templateUrl: './my-friend-page.component.html',
  styleUrls: ['./my-friend-page.component.css']
})
export class FriendsComponent implements OnInit {
  people: any;
  type: any = {
    friend: 'friend',
    sentRequest: 'sent',
    incomingRequest: 'incoming'
  };
  constructor(private friendService: FriendService, private store: Store<AppState>) { }

  async ngOnInit(): Promise<void> {
    await this.friendService.getPeople();
    this.store.select('people').subscribe(peopleU => this.people = peopleU);
  }
}
