import { User } from 'src/app/core/interfaces';
import { Component, Input, OnInit } from '@angular/core';
import { defaultUser } from 'src/app/shared/common/defaultValue';
import { FriendService } from 'src/app/core/services/friend.service';

@Component({
  selector: 'app-friend-item',
  templateUrl: './friend-item.component.html',
  styleUrls: ['./friend-item.component.css']
})
export class FriendComponent implements OnInit {
  @Input() user: User = defaultUser;
  @Input() type: string = '';
  constructor(private friendService: FriendService) { }

  ngOnInit(): void {
  }

  cancelFriendRequest(): void {
    this.friendService.cancelFriendRequest(this.user);
  }

  acceptFriendRequest(): void {
    this.friendService.acceptFriendRequest(this.user);
  }

  declineFriendRequest(): void {
    this.friendService.declineFriendRequest(this.user);
  }

  removeFriend(): void {
    this.friendService.removeFriend(this.user);
  }
}
