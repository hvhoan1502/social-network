import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { ChatService } from 'src/app/core/services/chat.service';

@Component({
  selector: 'app-chat-messages',
  templateUrl: './chat-messages.component.html',
  styleUrls: ['./chat-messages.component.css']
})

export class MessagesComponent implements OnInit {
  messages: Observable<string[]>;
  constructor(private chatService: ChatService,) { 
    this.messages = this.chatService.messages;
  }


  ngOnInit(): void {

  }

  
}
