import { User, AppState } from 'src/app/core/interfaces';
import { Component, OnInit , ElementRef } from '@angular/core';
import { Store } from '@ngrx/store';

import { UserService } from 'src/app/core/services/user.service';
import { ChatService } from 'src/app/core/services/chat.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-chat-page',
  templateUrl: './chat-page.component.html',
  styleUrls: ['./chat-page.component.css']
})
export class MessengerComponent implements OnInit {
  txtMessage: string = '';
  typingMessageObj: Observable<any>;
  isStopTyping: Observable<boolean> ;
  constructor(private userService: UserService,
              private chatService: ChatService
          ) {
            this.typingMessageObj = this.chatService.typingMessageObj;
            this.isStopTyping = this.chatService.isStopTyping;
          }

  ngOnInit(): void {
    this.userService.check();
    this.chatService.getAllRoomByUser();
  }

  sentMessage(): void {
    if (this.txtMessage.trim()) {
      this.chatService.createMessage(this.txtMessage);
      this.txtMessage = '';
    }
    this.stopTyping();
  }

  typing(): void {
    this.chatService.typingMessage();
  }

  stopTyping(event?: any): void {
    (!event || !event.target.value) && this.chatService.stopTyping();
  }
}
