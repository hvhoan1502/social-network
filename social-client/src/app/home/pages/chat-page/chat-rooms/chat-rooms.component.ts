import { Component, Input, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

import { AppState, People, Room } from 'src/app/core/interfaces';
import { defaultPeople } from 'src/app/shared/common/defaultValue';

@Component({
  selector: 'app-chat-rooms',
  templateUrl: './chat-rooms.component.html',
  styleUrls: ['./chat-rooms.component.css']
})
export class ChatRoomComponent implements OnInit {
  people: People = defaultPeople;
  rooms: any = [];
  idUser: string = '';
  constructor(private store: Store<AppState>) { }

  async ngOnInit(): Promise<void> {
    await this.store.select('user').subscribe((u: any) => this.idUser = u?._id);
    this.store.select('rooms').subscribe((result: any) => {
      const arrL = (result || []).length;
      for(let i = 0; i < arrL; i++) {
        this.rooms.push({
          _id: result[i]._id,
          author: result[i].author,
          createdAt: result[i].createdAt,
          updatedAt: result[i].updatedAt,
          friendInfo: result[i].users.find((u: any) => u._id != this.idUser)
        });
      }
    });
  }

  openChatRoom() {
    
  }

}
