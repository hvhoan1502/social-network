import { defaultUser } from 'src/app/shared/common/defaultValue';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

import { AppState, User } from 'src/app/core/interfaces';

@Component({
  selector: 'app-chat-header-info',
  templateUrl: './chat-header-info.component.html',
  styleUrls: ['./chat-header-info.component.css']
})
export class MessengerInfoComponent implements OnInit {
  user: User = defaultUser;
  constructor(private store: Store<AppState>) { }

  ngOnInit(): void {
    this.store.select('user').subscribe(user => this.user = user);
  }

}
