import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { UserService } from '../../../core/services/user.service';

@Component({
  selector: 'app-sign-in-page',
  templateUrl: './sign-in-page.component.html',
  styleUrls: ['./sign-in-page.component.css']
})
export class SignInComponent implements OnInit {
  formSignIn: FormGroup;
  isError: boolean = false;

  constructor(private fb: FormBuilder, private userService: UserService) {
    this.formSignIn = this.fb.group({
      email: ['', [Validators.required]],
      password: ['', [Validators.required]]
    })
  }

  ngOnInit(): void {
    
  }

  signIn() {
    const { email, password } = this.formSignIn.value;
    try {
      this.userService.signIn(email, password);
    } catch(err) {
      this.isError = true;
    }
  }

}
