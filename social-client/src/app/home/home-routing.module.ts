import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


import { PageNotFoundComponent } from './pages/not-found-page/not-found-page.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { MustBeGuestGuard } from '../core/guards/must-be-guest.guard';
import { MustBeUserGuard } from '../core/guards/must-be-user.guard';
import { ProfileComponent } from './pages/user-page/user-page.component';
import { FriendsComponent } from './pages/my-friend-page/my-friend-page.component';
import { SignInComponent } from './pages/sign-in-page/sign-in-page.component';
import { SignUpComponent } from './pages/sign-up-page/sign-up-page.component';
import { ForgotPasswordComponent } from './pages/forgot-password-page/forgot-password-page.component';
import { MessengerComponent } from './pages/chat-page/chat-page.component';
import { ProfileFormComponent } from './pages/user-page/user-form/user-form.component';

const routesConfig: Routes = [
  { path: '', component: HomePageComponent, canActivate: [MustBeUserGuard] },
  { path: 'profile', component: ProfileComponent, canActivate: [MustBeUserGuard]},
  { path: 'friends', component: FriendsComponent, canActivate: [MustBeUserGuard]},
  { path: 'login', component: SignInComponent, canActivate: [MustBeGuestGuard]},
  { path: 'register', component: SignUpComponent, canActivate: [MustBeGuestGuard]},
  { path: 'password', component: ForgotPasswordComponent, canActivate: [MustBeGuestGuard]},
  { path: 'messenger', component: MessengerComponent, canActivate: [MustBeUserGuard]},
  { path: 'profile/edit', component: ProfileFormComponent, canActivate: [MustBeUserGuard]},
  { path: '**', component: PageNotFoundComponent }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routesConfig)
  ],
  exports: [
    RouterModule
  ]
})
export class HomeRoutingModule { }
