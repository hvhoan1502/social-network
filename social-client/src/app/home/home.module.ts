import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { HomeRoutingModule } from './home-routing.module';

import { SignInComponent } from './pages/sign-in-page/sign-in-page.component';
import { SignUpComponent } from './pages/sign-up-page/sign-up-page.component';
import { StoriesComponent } from './pages/home-page/story-list/story-list.component';
import { ProfileComponent } from './pages/user-page/user-page.component';
import { PageNotFoundComponent } from './pages/not-found-page/not-found-page.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { FriendsComponent } from './pages/my-friend-page/my-friend-page.component';
import { ForgotPasswordComponent } from './pages/forgot-password-page/forgot-password-page.component';
import { InfoComponent } from './pages/user-page/user-info/user-info.component';
import { MyStoriesComponent } from './pages/user-page/user-story-list/user-story-list.component';
import { StoryComponent } from './pages/home-page/story-list/story-item/story-item.component';
import { ProfileFormComponent } from './pages/user-page/user-form/user-form.component';
import { CommentComponent } from './pages/home-page/story-list/story-item/comment-item/comment-item.component';
import { FriendComponent } from './pages/my-friend-page/friend-item/friend-item.component';
import { MessengerComponent } from './pages/chat-page/chat-page.component';
import { MessengerInfoComponent } from './pages/chat-page/chat-header-info/chat-header-info.component';
import { ChatRoomComponent } from './pages/chat-page/chat-rooms/chat-rooms.component';
import { MessagesComponent } from './pages/chat-page/chat-messages/chat-messages.component';
import { HeaderComponent } from '../shared/layout/header/header.component';
import { FooterComponent } from '../shared/layout/footer/footer.component';
import { CommentListComponent } from './pages/home-page/story-list/story-item/comment-list/comment-list.component';


@NgModule({
  declarations: [
    SignInComponent,
    SignUpComponent,
    StoriesComponent,
    ProfileComponent,
    PageNotFoundComponent,
    HomePageComponent,
    FriendsComponent,
    ForgotPasswordComponent,
    InfoComponent,
    MyStoriesComponent,
    StoryComponent,
    ProfileFormComponent,
    CommentComponent,
    FriendComponent,
    MessengerComponent,
    MessengerInfoComponent,
    ChatRoomComponent,
    MessagesComponent,
    HeaderComponent,
    FooterComponent,
    CommentListComponent,
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgxDropzoneModule
  ]
})

export class HomeModule { }
