import { User, People } from 'src/app/core/interfaces';

export const defaultUser: User = {
    _id: '',
    email: '',
    name: '',
    description: '',
    birthday: new Date(),
    stories: [],
    friends: [],
    incomingRequests: [],
    sentRequests: []
}

export const defaultPeople: People = {
    friends: [],
    incomingRequests: [],
    otherUsers: [],
    sentRequests: []
};