import { NgModule } from '@angular/core';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';

const socketIoConfig: SocketIoConfig = { url: 'http://localhost:3000', options: {}};

@NgModule({
  imports: [
    SocketIoModule.forRoot(socketIoConfig)
  ],
  exports: [
    SocketIoModule
  ]
})
export class AppSocketModule { }
