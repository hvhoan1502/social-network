import { Story } from "../interfaces";


export function myStoriesReducer(state: Story[] = [], action: any): Story[] {
    if (action.type === 'SET_MY_STORIES') return action.stories;
    return state;
}

export function storiesReducer(state: Story[] = [], action: any): Story[] {
    if (action.type === 'SET_STORIES') return action.stories;
    if (action.type === 'CREATE_STORY') return [action.story, ...state];
    if (action.type === 'CREATE_COMMENT') return state.map(story => {
        if (story._id !== action._id) return story;
        return { ...story, comments: [...story.comments, action.comment], commentNumber: story.comments.length + 1 };
    });
    if (action.type === 'LIKE_STORY') return state.map(story => {
        if (story._id !== action._id) return story;
        return {...story, fans: [action.user, ...story.fans ]};
    });
    if (action.type === 'DISLIKE_STORY') return state.map(story => {
        if (story._id !== action._id) return story;
        return {...story, fans: story.fans.filter(fan => fan._id !== action.idUser) };
    });
    if (action.type === 'LOAD_COMMENTS') return state.map(story => {
        if (story._id !== action.idStory) return story;
        return {
            ...story,
            comments: [...story.comments, ...action.comments],
            commentNumber: story.comments.length + action.comments.length
        }
    })
    if (action.type === 'LIKE_COMMENT') return state.map(story => {
        if (story._id !== action.idStory) return story;
        return {
            ...story,
            comments: story.comments.map(comment => {
                if (comment._id !== action.idComment) return comment;
                return { ...comment, fans: action.fans };
            })
        };
    });
    if (action.type === 'DISLIKE_COMMENT') return state.map((story: Story) => {
        if (story._id != action.idStory) return story;
        return {
            ...story,
            comments: story.comments.map(comment => {
                if (comment._id != action.idComment) return comment;
                return { ...comment, fans: action.fans };
            })
        }
    })
    if (action.type === 'REMOVE_STORY') return state.filter(story => story._id != action._id);

    return state;
}