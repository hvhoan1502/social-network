import { Room } from "../interfaces";

export function roomReducer(state: Room[], action: any) {
    if (action.type === 'SET_ROOMS') return action.rooms;
    if (action.type === 'CREATE_ROOM')  return [action.room, ...state];
    return state;
}

export function typingReducer(state: boolean = true, action: any) {
    if (action.type === 'STOP_TYPING') return false;
    return state;
}

export function messageReducer(state: Array<string>, action: any) {
    if (action.type === 'SET_MESSAGE') return action.messages;
    if (action.type === 'ADD_MESSAGE') return [action.message, ...state];
    return state;
}