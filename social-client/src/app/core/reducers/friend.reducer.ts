import { People } from "../interfaces";

export function friendInfoReducer(state = null, action: any) {
    if (action.type === 'SET_FRIEND_INFO') return action.info;
    return state;
}

const defaultPeople: People = { friends: [], incomingRequests: [], otherUsers: [], sentRequests: [] };

export function peopleReducer(state: People = defaultPeople, action: any): People {
    if (action.type === 'SET_PEOPLE') return action.people;
    if (action.type === 'ADD_FRIEND') {
        return {
            ...state,
            otherUsers: state.otherUsers.filter(user => user._id !== action.people.idUser),
            sentRequests: [action.people, ...state.sentRequests]
        };;
    }
    if (action.type === 'CANCEL_FRIEND') {
        return {
            ...state,
            sentRequests: [...state.sentRequests.filter(sr => sr._id != action.people._id)]
        }
    }
    if (action.type === 'ACCEPT_FRIEND') {
        return {
            ...state,
            friends: [action.people, ...state.friends],
            incomingRequests: [...state.incomingRequests.filter(sr => sr._id != action.people._id)]
        }
    }
    if (action.type === 'DECLINE_FRIEND') {
        return {
            ...state,
            incomingRequests: [...state.incomingRequests.filter(sr => sr._id != action.people._id)]
        };
    }
    if (action.type === 'REMOVE_FRIEND') {
        return {
            ...state,
            friends: [...state.friends.filter(sr => sr._id != action.people._id)]
        }
    }
    return state;
}