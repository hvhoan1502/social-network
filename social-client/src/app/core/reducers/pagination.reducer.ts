import { CATEGORY_PAGINATION, DEFAULT_PAGE_NUMBER } from "src/app/shared/common/constants";
import * as _ from 'lodash';
import { Story } from "../interfaces";


export function paginationReducer(state: any, action: any) {
    const categoryPaginate = action.category || '';
    switch(categoryPaginate) {
        case CATEGORY_PAGINATION.Comment:
            if (action.type === 'SET_COMMENT_PAGINATION') {
                if (!state) state = { comment: [] }
                action.stories.forEach((story: Story) => {
                    state[CATEGORY_PAGINATION.Comment].push({ 
                        storyId: story._id,
                        pageIndex: 1,
                        pageNumber: DEFAULT_PAGE_NUMBER
                    });
                });
                return state; 
            }
            if (action.type === 'SET_STORY_COMMENT_PAGINATION') {
                state[CATEGORY_PAGINATION.Comment] = (state[CATEGORY_PAGINATION.Comment] || []).push({ 
                    storyId: action._id,
                    pageIndex: 0,
                    pageNumber: DEFAULT_PAGE_NUMBER
                });
                return state;
            }
            if (action.type === 'LOAD_COMMENT_PAGINATION') {
                return state[CATEGORY_PAGINATION.Comment] = state[CATEGORY_PAGINATION.Comment].map((info: any) => {
                    if (info.storyId !== action._id) return info;
                    if (info.storyId === action._id) return _.merge(info, {
                        pageIndex: info.pageIndex ++,
                        pageNumber: info.pageNumber ++
                    })
                })
            }
            break;
        default:
            true
    }

    return state;
    
}