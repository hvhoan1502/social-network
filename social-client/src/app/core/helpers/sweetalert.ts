import Swal from 'sweetalert2';

export function getTitleByMessageType(type: string) {
    let title: string = ''
    switch (type) {
        case 'success':
            title = 'Success';
            break;
        case 'failed':
            title = 'Failed';
            break;
        case 'warning':
            title = 'Warning';
            break;
        default:
            title = ''
    }
    return title;
}


export function notification(message: string, type: any) {
  Swal.fire(getTitleByMessageType(type), message, type);
}
