import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';

import { AppState, UserInfo } from 'src/app/core/interfaces';
import { RequestService } from './request.service';
import { notification } from 'src/app/core/helpers/sweetalert';

@Injectable({
  providedIn: 'root',
})
export class FriendService {
  constructor(private request: RequestService, private store: Store<AppState>) {}

  getPeople() {
    return this.request
      .get('/friend')
      .then((response) =>
        this.store.dispatch({ type: 'SET_PEOPLE', people: response.people })
      )
      .catch((error) => notification(error.message, 'error'));
  }

  addFriend(user: UserInfo) {
    this.request
      .post('/friend/request/' + user._id, {})
      .then((response) => {
        this.store.dispatch({ type: 'ADD_FRIEND', people: response.receiver });
        notification('Sent Request', 'success');
      })
      .catch((error) => notification(error.message, 'error'));
  }

  cancelFriendRequest(user: UserInfo) {
    this.request
        .post('/friend/cancel/' + user._id, {})
        .then((response) => {
          this.store.dispatch({ type: 'CANCEL_FRIEND', people: response.receiver });
          notification('Cancelled this request!', 'success');
        })
        .catch(error => notification(error.message, 'error'));
  }

  acceptFriendRequest(user: UserInfo) {
    this.request
        .post('/friend/accept/' + user._id, {})
        .then((response) => {
          this.store.dispatch({ type: 'ACCEPT_FRIEND', people: response.friend });
          notification('Accepted this request!', 'success');
        })
        .catch(error => notification(error.message, 'error'));
  }

  declineFriendRequest(user: UserInfo) {
    this.request
        .post('/friend/decline/' + user._id, {})
        .then((response) => {
          this.store.dispatch({ type: 'DECLINE_FRIEND', people: response.requestor });
          notification('Declined this request', 'success');
        })
        .catch(error => notification(error.message, 'error'));
  }

  removeFriend(user: UserInfo) {
    this.request
        .post('/friend/remove/' + user._id, {})
        .then((response) => {
          this.store.dispatch({ type: 'REMOVE_FRIEND', people: response.friend });
          notification('Removed Friend', 'success');
        })
        .catch(error => notification(error.message, 'error'));
  }
}
