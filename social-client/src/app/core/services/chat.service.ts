import { Router } from '@angular/router';
import { Injectable, OnInit } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { Store } from '@ngrx/store';

import { RequestService } from './request.service';
import { notification } from 'src/app/core/helpers/sweetalert';

const SOCKET_ON_TYPES = {
  Typing: 'typing',
  StopTyping: 'stopTyping',
  ChatMessage: 'chatMessage'
}

const SOCKET_EMIT_TYPES = {
  ReceivedMessage: 'receivedMessage',
  NotifyTyping: 'notifyTyping',
  NotifyStopTyping: 'notifyStopTyping'
}

@Injectable({
  providedIn: 'root'
})
export class ChatService implements OnInit {
  messages = this.socket.fromEvent<string[]>(SOCKET_EMIT_TYPES.ReceivedMessage);
  typingMessageObj = this.socket.fromEvent<object>(SOCKET_EMIT_TYPES.NotifyTyping);
  isStopTyping = this.socket.fromEvent<boolean>(SOCKET_EMIT_TYPES.NotifyStopTyping);
  
  constructor(private socket: Socket,
              private request: RequestService,
              private store: Store,
              private router: Router) {}
  
  ngOnInit(): void {
    this.store.dispatch({ type: 'STOP_TYPING' })
  }

  createMessage(text: string) {
    this.socket.emit(SOCKET_ON_TYPES.ChatMessage, text);
  }

  stopTyping() {
    this.socket.emit(SOCKET_ON_TYPES.StopTyping, true);
  }

  typingMessage() {
    this.socket.emit(SOCKET_ON_TYPES.Typing, { user: 'Hoan', message: 'Hoan is typing...' });
  }

  createChatRoom(friendId: string) {
    this.request
    .post('/rooms', { friendId })
    .then((response) => {
      this.store.dispatch({ type: 'CREATE_ROOM', room: response.room });
      this.router.navigate(['/messenger']);
    })
    .catch((error) => notification(error.message, 'error'));
  }

  getAllRoomByUser() {
    this.request
    .get('/rooms')
    .then((response) => {
      this.store.dispatch({ type: 'SET_ROOMS', rooms: response.rooms });
    }).catch(error => notification(error.message, 'error'));
  }
}