import { Injectable, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';

import { RequestService } from './request.service';
import { AppState } from 'src/app/core/interfaces';
import { notification } from 'src/app/core/helpers/sweetalert';
import { CATEGORY_PAGINATION } from 'src/app/shared/common/constants';

@Injectable({
  providedIn: 'root',
})
export class StoryService {
  idUser: string = '';
  userName: string = '';
  constructor(
    private router: Router,
    private store: Store<AppState>,
    private request: RequestService
  ) {}

  ngOnInit(): void {}

  createStory(formData: FormData) {
    return this.request
      .post('/story', formData)
      .then((resJson) => {
        this.store.dispatch({ type: 'CREATE_STORY', story: resJson.story });
        notification('Story Created', 'success');
      })
      .catch((error) => notification(error.message, 'error'));
  }

  getAllStory() {
    this.request
      .get('/story')
      .then((response) => {
        this.store.dispatch({ type: 'SET_STORIES', stories: response.stories });
        this.store.dispatch({ type: 'SET_COMMENT_PAGINATION', stories: response.stories, category: CATEGORY_PAGINATION.Comment });
      })
      .catch((error) => notification(error.message, 'error'));
  }

  getStoryByAuthor(authorId: String) {
    this.request
      .get(`/story/${authorId}`)
      .then((response) => {
        this.store.dispatch({
          type: 'SET_MY_STORIES',
          stories: response.stories,
        });
      })
      .catch((error) => notification(error.message, 'error'));
  }

  likeStory(_id: string, userId: string) {
    this.request
      .post('/story/like/' + _id, {})
      .then((response) => {
        this.store.dispatch({
          _id,
          type: 'LIKE_STORY',
          user: { _id: userId, name: this.userName },
        });
      })
      .catch((error) => notification(error.message, 'error'));
  }

  dislikeStory(_id: string, userId: string) {
    this.request
      .post('/story/dislike/' + _id, {})
      .then((response) => {
        this.store.dispatch({
          _id,
          type: 'DISLIKE_STORY',
          idUser: userId,
        });
      })
      .catch((error) => notification(error.message, 'error'));
  }

  removeStory(_id: string) {
    this.request
      .delete('/story/' + _id)
      .then(response => {
        this.store.dispatch({
          _id,
          type: 'REMOVE_STORY'
        });
        notification('Story Removed', 'success');
      })
      .catch((error) => notification(error.message, 'error'));
  }
}
