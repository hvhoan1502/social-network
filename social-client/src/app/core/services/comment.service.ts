import { Injectable } from '@angular/core';
import { RequestService } from './request.service';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/core/interfaces';
import { notification } from 'src/app/core/helpers/sweetalert';

@Injectable({
  providedIn: 'root',
})
export class CommentService {
  constructor(private request: RequestService,
              private store: Store<AppState>) {}

  createComment(idStory: string, content: string) {
    this.request
      .post('/comment', { content, idStory })
      .then((response) =>
        this.store.dispatch({
          type: 'CREATE_COMMENT',
          _id: idStory,
          comment: response.comment,
        })
      )
      .catch((error) => notification(error.message, 'error'));
  }

  likeComment(idComment: string) {
    this.request
      .post('/comment/like/' + idComment, {})
      .then((response) => {
        const { _id, story, fans } = response.comment;
        this.store.dispatch({
          type: 'LIKE_COMMENT',
          idStory: story,
          idComment: _id,
          fans,
        });
      })
      .catch((error) => notification(error.message, 'error'));
  }

  dislikeComment(idComment: string) {
    this.request
      .post('/comment/dislike/' + idComment, {})
      .then((response) => {
        const { _id, story, fans } = response.comment;
        this.store.dispatch({
          type: 'DISLIKE_COMMENT',
          idStory: story,
          idComment: _id,
          fans
        })
      })
      .catch((error) => notification(error.message, 'error'));
  }

  loadComments(storyId: string, pageIndex:number, pageNumber: number) {
      this.request
        .post('/comment/search', { storyId, pageIndex, pageNumber })
        .then((response) => {
          this.store.dispatch({
            type: 'LOAD_COMMENTS',
            idStory: storyId,
            comments: response.comments
          })
        })
        .catch((error) => notification(error.message, 'error'));
  }
}
