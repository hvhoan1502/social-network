import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';

import { AppState } from 'src/app/core/interfaces';
import { RequestService } from './request.service';
import { decode } from 'jsonwebtoken';
import { notification } from 'src/app/core/helpers/sweetalert';

@Injectable()
export class UserService {
  constructor(
    private router: Router,
    private store: Store<AppState>,
    private request: RequestService
  ) {}

  signIn(email: string, password: string) {
    return this.request
      .post('/user/signin', { email, password })
      .then((resJson) => {
        this.store.dispatch({ type: 'SET_USER', user: resJson.user });
        localStorage.setItem('token', resJson.user.token);
        this.router.navigate(['/']);
      })
      .catch((error) => {
        notification(error.message, 'error');
      });
  }

  updateProfile(body: object) {
    return this.request
      .put('/user/profile', body)
      .then((resJson) => {
        this.store.dispatch({ type: 'SET_USER', user: resJson.user });
        localStorage.setItem('token', resJson.user.token);
        this.router.navigate(['/profile']);
      }).catch((error) => notification(error.message, 'error'));
  }

  singUp(name: string, email: string, password: string, birthday: Date) {
    return this.request
        .post('/user/signup', { name, email, password, birthday }) 
        .then((resJson) => {
            notification('Created user, please login', 'success');
            this.router.navigate(['/login']);
        })
        .catch((error) => notification(error.message, 'error'));
  }

  getUserById(userId: string) {
    return this.request
        .get('/user/' + userId)
        .then(resJson => {
          this.store.dispatch({ type: 'SET_FRIEND_INFO', info: resJson.user });
        })
        .catch(error => notification(error.message, 'error'));
  }

  verifyToken(): boolean {
    try {
      const token = localStorage.getItem('token');
      if (!token) return false;
      const decoded: any = decode(token);
      if (!decode) return false;
      if (decoded.exp * 1000 < Date.now()) return false;
      return true;
    } catch (error) {
      return false;
    }
  }

  check() {
    if (!this.verifyToken()) {
      localStorage.removeItem('token');
      this.store.dispatch({ type: 'LOADED' })
      this.router.navigate(['login']); 
    }
    return this.request
      .get('/user/check')
      .then((resJson) => {
        this.store.dispatch({ type: 'SET_USER', user: resJson.user });
        localStorage.setItem('token', resJson.user.token);
      })
      .catch((error) => {
        localStorage.removeItem('token')
        notification(error.message, 'error')
      })
      .then(() => this.store.dispatch({ type: 'LOADED' }));
  }

  logOut() {
    this.store.dispatch({ type: 'LOG_OUT' });
    localStorage.removeItem('token');
    this.router.navigate(['/login']);
  }
}
