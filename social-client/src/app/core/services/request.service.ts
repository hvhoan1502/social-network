import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const SERVER_URL = 'http://localhost:3000/api';


@Injectable({
  providedIn: 'root',
})

export class RequestService {
  constructor(private http: HttpClient) {}

  processResponse(response: Promise<any>) {
    return response
      .then(
        (res) => res,
        (error) => error.error
      )
      .then((resJson) => {
        if (!resJson.success) { 
          throw new Error(resJson.message);
        }
        return resJson;
      });
  }

  get(subUrl: string) {
    const headers = new HttpHeaders({ token: localStorage.getItem('token') || '' });
    const response = this.http
      .get(SERVER_URL + subUrl, { headers })
      .toPromise();
    return this.processResponse(response);
  }

  post(subUrl: string, body: object) {
    const headers = new HttpHeaders({ token: localStorage.getItem('token') || '' });
    const response = this.http
      .post(SERVER_URL + subUrl, body, { headers })
      .toPromise();
    return this.processResponse(response);
  }

  put(subUrl: string, body: object) {
    const headers = new HttpHeaders({ token: localStorage.getItem('token') || '' });
    const response = this.http
      .put(SERVER_URL + subUrl, body, { headers })
      .toPromise();
    return this.processResponse(response);
  }

  delete(subUrl: string) {
    const headers = new HttpHeaders({ token: localStorage.getItem('token') || '' });
    const response = this.http
      .delete(SERVER_URL + subUrl, { headers })
      .toPromise();
    return this.processResponse(response);
  }
}