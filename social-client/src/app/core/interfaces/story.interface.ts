import { UserInfo } from "./user.interface";
import { Comment } from "./comment.interface";

export interface Story {
    _id: string;
    content: string;
    image: string; 
    author?: UserInfo;
    fans: UserInfo[];
    commentNumber: number;
    comments: Comment[];
    created_date?: Date;
}

