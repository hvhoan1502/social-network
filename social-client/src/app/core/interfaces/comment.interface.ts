import { UserInfo } from "./user.interface";

export interface Comment {
    _id: string;
    author: UserInfo;
    fans: string[];
    content: string;
}