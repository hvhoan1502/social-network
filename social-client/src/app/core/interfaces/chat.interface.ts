import { UserInfo } from "./user.interface";


export interface Message {
    _id: string;
    message: string;
    author: UserInfo;
    readByRecipients: UserInfo[];
} 

export interface Room {
    _id: string;
    users: Array<UserInfo>;
    author: UserInfo;
    messages: Array<Message>
    createdAt: Date;
    updatedAt: Date;
}