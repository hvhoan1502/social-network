import { Story } from "./story.interface";


export interface User {
    _id: string;
    name: string;
    email: string;
    avatar?: string;
    birthday?: Date;
    description?: string;
    stories: Story[];
    friends: UserInfo[];
    incomingRequests: UserInfo[];
    sentRequests: UserInfo[];

}

export interface UserInfo {
    _id: string;
    name: string;
    avatar?: string;
}

export interface People {
    friends: UserInfo[];
    incomingRequests: UserInfo[];
    sentRequests: UserInfo[];
    otherUsers: UserInfo[];
}