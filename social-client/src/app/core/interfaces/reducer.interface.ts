import { Room } from "./chat.interface";
import { Story } from "./story.interface";
import { People, User } from "./user.interface";

export interface AppState { 
    loading: boolean;
    stories: Story[];
    user: User;
    myStories: Story[];
    people: People[];
    friendInfo: User;
    rooms: Room[];
    paginate: any;
}