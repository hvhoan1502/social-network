import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { HomeModule } from './home/home.module';
import { AppStoreModule } from './app-store.module';
import { AppSocketModule } from './app-socket.module';
import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { UserService } from './core/services/user.service';



@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HomeModule,
    AppStoreModule,
    AppSocketModule,
    RouterModule
  ],
  providers: [
    UserService
  ],
  bootstrap: [
    AppComponent
  ]
})

export class AppModule {
  constructor() {
  }
}
