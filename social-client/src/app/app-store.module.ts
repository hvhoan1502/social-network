import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import * as stores from './core/reducers';


const storesConfig = { 
    user: stores.userReducer,
    friendInfo: stores.friendInfoReducer,
    stories:  stores.storiesReducer,
    loading: stores.loadingReducer,
    myStories: stores.myStoriesReducer,
    people: stores.peopleReducer,
    rooms: stores.roomReducer,
    paginate: stores.paginationReducer
  }

@NgModule({
  imports: [
    StoreModule.forRoot(storesConfig)
  ],
  exports: [
    StoreModule
  ]
})
export class AppStoreModule { }
